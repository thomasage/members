<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsCommand(
    name: 'app:user:add',
    description: 'Add an user',
)]
final class UserAddCommand extends Command
{
    public function __construct(
        private readonly UserPasswordHasherInterface $passwordHasher,
        private readonly EntityManagerInterface $entityManager,
        private readonly UserRepository $userRepository,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $helper = $this->getHelper('question');

        $question = new Question('Username: ');
        $username = (string) $helper->ask($input, $output, $question);
        if ('' === $username) {
            $io->error('Username is required.');

            return Command::FAILURE;
        }
        $found = $this->userRepository->findOneBy(['username' => $username]);
        if ($found) {
            $io->error('Username already exists.');

            return Command::FAILURE;
        }

        $question = new Question('Password: ');
        $question->setHidden(true);
        $question->setHiddenFallback(false);
        $password = (string) $helper->ask($input, $output, $question);
        if ('' === $password) {
            $io->error('Password is required.');

            return Command::FAILURE;
        }

        $user = new User();
        $user
            ->setPassword($this->passwordHasher->hashPassword($user, $password))
            ->setUsername($username);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $io->success('User added.');

        return Command::SUCCESS;
    }
}
