<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\PaymentMethod;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

final class PaymentMethodFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $method = new PaymentMethod();
        $method->setName('Chèque');
        $manager->persist($method);

        $method = new PaymentMethod();
        $method->setName('Espèces');
        $manager->persist($method);

        $method = new PaymentMethod();
        $method->setName('Virement');
        $manager->persist($method);

        $method = new PaymentMethod();
        $method->setName('Prélèvement');
        $manager->persist($method);

        $manager->flush();
    }
}
