<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Attendance;
use App\Entity\Lesson;
use App\Entity\Member;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

final class AttendanceFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        /** @var Lesson $lesson */
        $lesson = $this->getReference('lesson');
        /** @var Member $memberLeonard */
        $memberLeonard = $this->getReference('member_leonard');
        /** @var Member $memberSheldon */
        $memberSheldon = $this->getReference('member_sheldon');

        $attendance = Attendance::createFromLessonAndMember($lesson, $memberSheldon);
        $attendance->setState(Attendance::STATE_PRESENT);
        $manager->persist($attendance);

        $attendance = Attendance::createFromLessonAndMember($lesson, $memberLeonard);
        $attendance->setState(Attendance::STATE_APOLOGY);
        $manager->persist($attendance);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            LessonFixtures::class,
            MemberFixtures::class,
        ];
    }
}
