<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Theme;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

final class ThemeFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $theme = new Theme('Kata');
        $manager->persist($theme);

        $theme = new Theme('Self-défense');
        $manager->persist($theme);

        $theme = new Theme('Sol');
        $manager->persist($theme);

        $manager->flush();
    }
}
