<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Lesson;
use App\Entity\Level;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

final class LessonFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        /** @var Level $levelAdults */
        $levelAdults = $this->getReference('level_adults');

        $lesson = new Lesson();
        $lesson
            ->addLevel($levelAdults)
            ->setDurationInMinutes(120)
            ->setStartAt(new DateTimeImmutable('2022-06-23 19:00:00'));
        $manager->persist($lesson);
        $this->setReference('lesson', $lesson);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            LevelFixtures::class,
        ];
    }
}
