<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Level;
use App\Entity\Member;
use App\Entity\Membership;
use App\Entity\Season;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

final class MembershipFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        /** @var Level $level */
        $level = $this->getReference('level_adults');
        /** @var Member $memberLeonard */
        $memberLeonard = $this->getReference('member_leonard');
        /** @var Member $memberPenny */
        $memberPenny = $this->getReference('member_penny');
        /** @var Member $memberSheldon */
        $memberSheldon = $this->getReference('member_sheldon');
        /** @var Season $season */
        $season = $this->getReference('season_2021');

        $membership = new Membership();
        $membership
            ->setLevel($level)
            ->setMember($memberLeonard)
            ->setSeason($season);
        $manager->persist($membership);

        $membership = new Membership();
        $membership
            ->setLevel($level)
            ->setMember($memberPenny)
            ->setSeason($season);
        $manager->persist($membership);

        $membership = new Membership();
        $membership
            ->setLevel($level)
            ->setMember($memberSheldon)
            ->setSeason($season);
        $manager->persist($membership);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            LevelFixtures::class,
            MemberFixtures::class,
            SeasonFixtures::class,
        ];
    }
}
