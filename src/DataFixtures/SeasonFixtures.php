<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Season;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

final class SeasonFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $season = new Season();
        $season
            ->setEnd(new DateTimeImmutable('2022-06-28'))
            ->setStart(new DateTimeImmutable('2021-08-31'));
        $manager->persist($season);
        $this->setReference('season_2021', $season);

        $manager->flush();
    }
}
