<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Level;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

final class LevelFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $level = new Level();
        $level->setName('Enfants');
        $manager->persist($level);

        $level = new Level();
        $level->setName('Adolescents / Adultes');
        $manager->persist($level);
        $this->setReference('level_adults', $level);

        $manager->flush();
    }
}
