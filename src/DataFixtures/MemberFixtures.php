<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Member;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

final class MemberFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $member = new Member();
        $member
            ->setBirthday(new DateTimeImmutable('1980-02-26'))
            ->setCity('Pasadena')
            ->setFirstName('Sheldon')
            ->setGender('m')
            ->setLastName('Lee Cooper')
            ->setPostCode('06-56000')
            ->setStreetAddress('2311 North Los Robless Avenue');
        $manager->persist($member);
        $this->setReference('member_sheldon', $member);

        $member = new Member();
        $member
            ->setBirthday(new DateTimeImmutable('1980-04-22'))
            ->setCity('Pasadena')
            ->setFirstName('Leonard')
            ->setGender('m')
            ->setLastName('Hofstadter')
            ->setPostCode('06-56000')
            ->setStreetAddress('2311 North Los Robless Avenue');
        $manager->persist($member);
        $this->setReference('member_leonard', $member);

        $member = new Member();
        $member
            ->setBirthday(new DateTimeImmutable('1985-12-02'))
            ->setCity('Pasadena')
            ->setFirstName('Penny')
            ->setGender('f')
            ->setLastName('NoLastName')
            ->setPostCode('06-56000')
            ->setStreetAddress('2311 North Los Robless Avenue');
        $manager->persist($member);
        $this->setReference('member_penny', $member);

        $manager->flush();
    }
}
