<?php

declare(strict_types=1);

namespace App\Controller\Statistics;

use App\Form\StatisticsMembershipNumberType;
use App\Repository\MembershipRepository;
use App\Repository\SeasonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

final class StatisticsMembershipNumberController extends AbstractController
{
    public function __construct(
        private readonly MembershipRepository $membershipRepository,
        private readonly SeasonRepository $seasonRepository,
    ) {
    }

    #[Route(
        '/statistics/membership/number',
        name: 'app_statistics_membership_number',
        methods: ['GET']
    )]
    public function __invoke(Request $request): Response
    {
        $lastSeason = $this->seasonRepository->findLatest();
        if (!$lastSeason) {
            return $this->redirectToRoute('app_statistics_index');
        }

        $form = $this->createForm(StatisticsMembershipNumberType::class, [
            'season' => (string) $lastSeason->getUuid(),
        ]);
        $form->handleRequest($request);

        $results = $this->membershipRepository->statisticsNumber(Uuid::fromString($form->getData()['season']));
        $total = array_reduce($results, static fn (int $total, array $result): int => $total + $result['total'], 0);

        return $this->render('statistics/membership_number.html.twig', [
            'form' => $form->createView(),
            'results' => $results,
            'total' => $total,
        ]);
    }
}
