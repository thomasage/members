<?php

declare(strict_types=1);

namespace App\Controller\Statistics;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class StatisticsIndexController extends AbstractController
{
    #[Route(
        '/statistics',
        name: 'app_statistics_index',
        methods: ['GET']
    )]
    public function __invoke(): Response
    {
        return $this->render('statistics/index.html.twig');
    }
}
