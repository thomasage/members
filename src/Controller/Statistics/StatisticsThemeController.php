<?php

declare(strict_types=1);

namespace App\Controller\Statistics;

use App\Entity\Lesson;
use App\Form\StatisticsThemeSearchType;
use App\Repository\LessonRepository;
use App\Repository\LevelRepository;
use App\Repository\SeasonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

final class StatisticsThemeController extends AbstractController
{
    private const CHART_COLORS = [
        '120,120,255',
        '0,220,0',
        '0,240,240',
        '255,100,100',
        '255,100,255',
        '255,255,0',
    ];

    public function __construct(
        private readonly ChartBuilderInterface $chartBuilder,
        private readonly LessonRepository $lessonRepository,
        private readonly LevelRepository $levelRepository,
        private readonly SeasonRepository $seasonRepository,
    ) {
    }

    #[Route(
        '/statistics/theme',
        name: 'app_statistics_theme',
        methods: ['GET']
    )]
    public function __invoke(Request $request): Response
    {
        $lastSeason = $this->seasonRepository->findOneBy([], ['start' => 'DESC']);
        if (!$lastSeason) {
            return $this->redirectToRoute('app_statistics_index');
        }
        $firstLevel = $this->levelRepository->findOneBy([], ['name' => 'ASC']);
        if (!$firstLevel) {
            return $this->redirectToRoute('app_statistics_index');
        }

        $form = $this->createForm(
            StatisticsThemeSearchType::class,
            [
                'level' => (string) $firstLevel->getUuid(),
                'season' => (string) $lastSeason->getUuid(),
            ],
            ['method' => 'GET']
        );
        $form->handleRequest($request);

        $season = $this->seasonRepository->findByUuid(Uuid::fromString($form->getData()['season']));
        if (!$season) {
            return $this->redirectToRoute('app_statistics_index');
        }

        $lessons = $this->lessonRepository->findStatisticsThemes(
            $season->getStart(),
            $season->getEnd(),
            Uuid::fromString($form->getData()['level'])
        );

        $data = $this->buildData($lessons);
        $chart = $this->buildChart($data);

        return $this->render('statistics/theme.html.twig', [
            'chart' => $chart,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Lesson[] $lessons
     *
     * @return array{datasets:array<array{backgroundColor:string,data:array<float|null>,label:string}>,labels:string[]}
     */
    private function buildData(array $lessons): array
    {
        $themes = [];
        foreach ($lessons as $lesson) {
            foreach ($lesson->getThemes() as $theme) {
                if (!in_array($theme, $themes, true)) {
                    $themes[] = $theme;
                }
            }
        }

        $data = [
            'datasets' => [],
            'labels' => [],
        ];
        $init = array_fill_keys(array_map(static fn (Lesson $lesson): int => (int) $lesson->getId(), $lessons), null);
        foreach ($themes as $t => $theme) {
            $data['datasets'][$theme->getId()] = [
                'backgroundColor' => 'rgba('.self::CHART_COLORS[$t].',0.8)',
                'data' => $init,
                'label' => $theme->getName(),
            ];
        }
        foreach ($lessons as $lesson) {
            $data['labels'][] = $lesson->getStartAt()->format('Y-m-d');
            $count = count($lesson->getThemes());
            foreach ($lesson->getThemes() as $theme) {
                $data['datasets'][$theme->getId()]['data'][$lesson->getId()] = round(1 / $count, 5);
            }
        }
        foreach ($data['datasets'] as $themeId => $themeData) {
            $data['datasets'][$themeId]['data'] = array_values($themeData['data']);
        }
        $data['datasets'] = array_values($data['datasets']);

        return $data;
    }

    /**
     * @param array{datasets:array<array{backgroundColor:string,data:array<float|null>,label:string}>,labels:string[]} $data
     */
    private function buildChart(array $data): Chart
    {
        $chart = $this->chartBuilder->createChart(Chart::TYPE_BAR);
        $chart->setData($data);
        $chart->setOptions([
            'scales' => [
                'x' => [
                    'stacked' => true,
                ],
                'y' => [
                    'stacked' => true,
                ],
            ],
        ]);

        return $chart;
    }
}
