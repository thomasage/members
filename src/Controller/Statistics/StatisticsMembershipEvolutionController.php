<?php

declare(strict_types=1);

namespace App\Controller\Statistics;

use App\Repository\MembershipRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

final class StatisticsMembershipEvolutionController extends AbstractController
{
    private const CHART_COLORS = [
        'rgba(120,120,255,0.8)',
        'rgba(0,220,0,0.8)',
        'rgba(0,240,240,0.8)',
        'rgba(255,100,100,0.8)',
        'rgba(255,100,255,0.8)',
        'rgba(255,255,0,0.8)',
    ];

    public function __construct(
        private readonly ChartBuilderInterface $chartBuilder,
        private readonly MembershipRepository $membershipRepository,
    ) {
    }

    #[Route(
        '/statistics/membership/evolution',
        name: 'app_statistics_membership_evolution',
        methods: ['GET']
    )]
    public function __invoke(Request $request): Response
    {
        $members = $this->membershipRepository->findStatisticsEvolution();

        $data = $this->buildData($members);
        $chart = $this->buildChart($data);

        return $this->render('statistics/membership_evolution.html.twig', [
            'chart' => $chart,
        ]);
    }

    /**
     * @param array<array{level_name:string,season_start:int,total:int}> $results
     *
     * @return array{datasets:array<array{backgroundColor:string,data:array<int|null>,label:string}>,labels:array<int>}
     */
    private function buildData(array $results): array
    {
        $levels = array_map(static fn (array $result): string => $result['level_name'], $results);
        $levels = array_unique($levels);
        sort($levels);
        $levels = array_flip($levels);
        $seasons = array_map(static fn (array $result): int => $result['season_start'], $results);
        $seasons = array_unique($seasons);
        sort($seasons);
        $data = [
            'datasets' => [],
            'labels' => $seasons,
        ];
        $init = array_fill_keys($seasons, null);
        foreach ($results as $result) {
            if (!isset($data['datasets'][$result['level_name']])) {
                $data['datasets'][$result['level_name']] = [
                    'backgroundColor' => self::CHART_COLORS[$levels[$result['level_name']]] ?? '',
                    'data' => $init,
                    'label' => $result['level_name'],
                ];
            }
            $data['datasets'][$result['level_name']]['data'][$result['season_start']] = $result['total'];
        }
        foreach ($data['datasets'] as $levelName => $levelData) {
            $data['datasets'][$levelName]['data'] = array_values($levelData['data']);
        }
        $data['datasets'] = array_values($data['datasets']);

        return $data;
    }

    /**
     * @param array{datasets:array<array{backgroundColor:string,data:array<int|null>,label:string}>,labels:array<int>} $data
     */
    private function buildChart(array $data): Chart
    {
        $chart = $this->chartBuilder->createChart(Chart::TYPE_BAR);
        $chart->setData($data);
        $chart->setOptions([
            'scales' => [
                'xAxes' => [
                    [
                        'stacked' => true,
                    ],
                ],
                'yAxes' => [
                    [
                        'stacked' => true,
                        'ticks' => [
                            'min' => 0,
                        ],
                    ],
                ],
            ],
        ]);

        return $chart;
    }
}
