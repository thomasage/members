<?php

declare(strict_types=1);

namespace App\Controller\Statistics;

use App\Form\StatisticsCitySearchType;
use App\Repository\MemberRepository;
use App\Repository\SeasonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

final class StatisticsCityController extends AbstractController
{
    private const CHART_COLORS = [
        'rgba(120,120,255,0.8)',
        'rgba(0,220,0,0.8)',
        'rgba(0,240,240,0.8)',
        'rgba(255,100,100,0.8)',
        'rgba(255,100,255,0.8)',
        'rgba(255,255,0,0.8)',
    ];

    public function __construct(
        private readonly ChartBuilderInterface $chartBuilder,
        private readonly MemberRepository $memberRepository,
        private readonly SeasonRepository $seasonRepository,
    ) {
    }

    #[Route(
        '/statistics/city',
        name: 'app_statistics_city',
        methods: ['GET']
    )]
    public function __invoke(Request $request): Response
    {
        $lastSeason = $this->seasonRepository->findOneBy([], ['start' => 'DESC']);
        if (!$lastSeason) {
            return $this->redirectToRoute('app_statistics_index');
        }

        $form = $this->createForm(
            StatisticsCitySearchType::class,
            ['season' => (string) $lastSeason->getUuid()],
            ['method' => 'GET']
        );
        $form->handleRequest($request);

        $season = $this->seasonRepository->findByUuid(Uuid::fromString($form->getData()['season']));
        if (!$season) {
            return $this->redirectToRoute('app_statistics_index');
        }

        $members = $this->memberRepository->findStatisticsCities($season);

        $data = $this->buildData($members);
        $chart = $this->buildChart($data);

        return $this->render('statistics/city.html.twig', [
            'chart' => $chart,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param array<array{city:string,total:int}> $results
     *
     * @return array{datasets:array<array{backgroundColor:array<string>,data:array<int>}>,labels:array<string>}
     */
    private function buildData(array $results): array
    {
        $data = [
            'datasets' => [
                [
                    'backgroundColor' => self::CHART_COLORS,
                    'data' => [],
                ],
            ],
            'labels' => [],
        ];
        foreach ($results as $result) {
            $data['datasets'][0]['data'][] = $result['total'];
            $data['labels'][] = $result['city'];
        }

        return $data;
    }

    /**
     * @param array{datasets:array<array{backgroundColor:array<string>,data:array<int>}>,labels:array<string>} $data
     */
    private function buildChart(array $data): Chart
    {
        $chart = $this->chartBuilder->createChart(Chart::TYPE_DOUGHNUT);
        $chart->setData($data);

        return $chart;
    }
}
