<?php

declare(strict_types=1);

namespace App\Controller\Membership;

use App\Entity\Membership;
use App\Form\MembershipType;
use App\Repository\MemberRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\Translation\TranslatorInterface;

final class MembershipAddController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly MemberRepository $memberRepository,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route(
        '/membership/add/{memberUuid}',
        name: 'app_membership_add',
        methods: ['GET', 'POST']
    )]
    public function __invoke(Request $request, string $memberUuid): Response
    {
        $member = $this->memberRepository->findByUuid(Uuid::fromString($memberUuid));
        if (!$member) {
            throw new NotFoundHttpException();
        }

        $membership = new Membership();
        $membership->setMember($member);

        $form = $this->createForm(MembershipType::class, $membership);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($membership);
            $this->entityManager->flush();

            $this->addFlash('success', $this->translator->trans('notification.membership_added'));

            return $this->redirectToRoute('app_member_show', [
                'uuid' => $member->getUuid(),
            ]);
        }

        return $this->render('membership/add.html.twig', [
            'form' => $form->createView(),
            'member' => $member,
        ]);
    }
}
