<?php

declare(strict_types=1);

namespace App\Controller\Membership;

use App\Form\MembershipType;
use App\Repository\MembershipRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\Translation\TranslatorInterface;

final class MembershipEditController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly MembershipRepository $membershipRepository,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route(
        '/membership/edit/{uuid}',
        name: 'app_membership_edit',
        methods: ['GET', 'POST']
    )]
    public function __invoke(Request $request, string $uuid): Response
    {
        $membership = $this->membershipRepository->findByUuid(Uuid::fromString($uuid));
        if (!$membership) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(MembershipType::class, $membership);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            $this->addFlash('success', $this->translator->trans('notification.membership_updated'));

            return $this->redirectToRoute('app_member_show', [
                'uuid' => $membership->getMember()->getUuid(),
            ]);
        }

        return $this->render('membership/edit.html.twig', [
            'form' => $form->createView(),
            'membership' => $membership,
        ]);
    }
}
