<?php

declare(strict_types=1);

namespace App\Controller\Lesson;

use App\Form\LessonType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

final class LessonAddController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route(
        '/lesson/add',
        name: 'app_lesson_add',
        methods: ['GET', 'POST']
    )]
    public function __invoke(Request $request): Response
    {
        $form = $this->createForm(LessonType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $lesson = $form->getData();
            $this->entityManager->persist($lesson);
            $this->entityManager->flush();

            $this->addFlash('success', $this->translator->trans('notification.lesson_added'));

            return $this->redirectToRoute('app_lesson_edit', [
                'uuid' => (string) $lesson->getUuid(),
            ]);
        }

        return $this->render('lesson/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
