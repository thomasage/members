<?php

declare(strict_types=1);

namespace App\Controller\Lesson;

use App\Calendar\Calendar;
use App\Repository\LessonRepository;
use App\Repository\SeasonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

final class LessonIndexController extends AbstractController
{
    public function __construct(
        private readonly LessonRepository $lessonRepository,
        private readonly SeasonRepository $seasonRepository,
    ) {
    }

    #[Route(
        '/lesson',
        name: 'app_lesson_index',
        methods: ['GET']
    )]
    public function __invoke(Request $request): Response
    {
        $season = $this->seasonRepository->findLatest();
        if (!$season) {
            throw new NotFoundHttpException();
        }

        $calendar = new Calendar($season->getStart(), $season->getEnd());
        $daysWithLesson = $this->lessonRepository->findDaysWithAtLeastOneLesson($season->getStart(), $season->getEnd());

        return $this->render('lesson/index.html.twig', [
            'calendar' => $calendar,
            'daysWithLesson' => $daysWithLesson,
            'season' => $season,
        ]);
    }
}
