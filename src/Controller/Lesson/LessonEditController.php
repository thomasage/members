<?php

declare(strict_types=1);

namespace App\Controller\Lesson;

use App\Form\LessonType;
use App\Repository\LessonRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\Translation\TranslatorInterface;

final class LessonEditController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly LessonRepository $lessonRepository,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route(
        '/lesson/edit/{uuid}',
        name: 'app_lesson_edit',
        methods: ['GET', 'POST']
    )]
    public function __invoke(Request $request, string $uuid): Response
    {
        $lesson = $this->lessonRepository->findByUuid(Uuid::fromString($uuid));
        if (!$lesson) {
            throw new NotFoundHttpException('Lesson not found');
        }

        $form = $this->createForm(LessonType::class, $lesson);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            $this->addFlash('success', $this->translator->trans('notification.lesson_updated'));

            return $this->redirectToRoute('app_lesson_edit', [
                'uuid' => (string) $lesson->getUuid(),
            ]);
        }

        return $this->render('lesson/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
