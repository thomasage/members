<?php

declare(strict_types=1);

namespace App\Controller\Lesson;

use App\Form\LessonDeleteType;
use App\Repository\LessonRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\Translation\TranslatorInterface;

final class LessonDeleteController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly LessonRepository $lessonRepository,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route(
        '/lesson/delete/{uuid}',
        name: 'app_lesson_delete',
        methods: ['GET', 'POST']
    )]
    public function __invoke(Request $request, string $uuid): Response
    {
        $lesson = $this->lessonRepository->findByUuid(Uuid::fromString($uuid));
        if (!$lesson) {
            throw new NotFoundHttpException('Lesson not found');
        }

        $form = $this->createForm(LessonDeleteType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $lesson->setActive(false);
            $this->entityManager->flush();

            $this->addFlash('success', $this->translator->trans('notification.lesson_deleted'));

            return $this->redirectToRoute('app_lesson_date', [
                'date' => $lesson->getStartAt()->format('Y-m-d'),
            ]);
        }

        return $this->render('lesson/delete.html.twig', [
            'form' => $form->createView(),
            'lesson' => $lesson,
        ]);
    }
}
