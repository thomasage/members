<?php

declare(strict_types=1);

namespace App\Controller\Lesson;

use App\Form\LessonDateType;
use App\Repository\LessonRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

final class LessonDateController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly LessonRepository $lessonRepository,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route(
        '/lesson/date/{date}',
        name: 'app_lesson_date',
        methods: ['GET', 'POST']
    )]
    public function __invoke(Request $request, string $date): Response
    {
        $dateTime = DateTimeImmutable::createFromFormat('Y-m-d', $date);
        if (!$dateTime) {
            throw new NotFoundHttpException();
        }

        $lessons = $this->lessonRepository->findActiveByDate($dateTime);

        $form = $this->createForm(LessonDateType::class, ['lessons' => $lessons]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            $this->addFlash('success', $this->translator->trans('notification.attendances_updated'));

            return $this->redirectToRoute('app_lesson_date', ['date' => $date]);
        }

        return $this->render('lesson/date.html.twig', [
            'date' => $dateTime,
            'form' => $form->createView(),
        ]);
    }
}
