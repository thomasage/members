<?php

declare(strict_types=1);

namespace App\Controller\Accounting\Transaction;

use App\Form\TransactionEditType;
use App\Repository\TransactionDetailRepository;
use App\Repository\TransactionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\Translation\TranslatorInterface;

final class TransactionEditController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly TransactionDetailRepository $transactionDetailRepository,
        private readonly TransactionRepository $transactionRepository,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route(
        '/accounting/transaction/edit/{uuid}',
        name: 'app_accounting_transaction_edit',
        methods: ['GET', 'POST']
    )]
    public function __invoke(Request $request, string $uuid): Response
    {
        $transaction = $this->transactionRepository->findByUuid(Uuid::fromString($uuid));
        if (!$transaction) {
            throw new NotFoundHttpException();
        }

        $originalDetails = new ArrayCollection();
        foreach ($transaction->getDetails() as $detail) {
            $originalDetails->add($detail);
        }

        $form = $this->createForm(TransactionEditType::class, $transaction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($originalDetails as $detail) {
                if ($transaction->getDetails()->contains($detail)) {
                    continue;
                }
                $this->entityManager->remove($detail);
            }
            $this->entityManager->flush();

            $this->addFlash('success', $this->translator->trans('notification.transaction_updated'));

            if ($request->request->has('save_and_close')) {
                return $this->redirectToRoute('app_accounting_transaction_index');
            }

            return $this->redirectToRoute('app_accounting_transaction_edit', ['uuid' => $uuid]);
        }

        $bankNames = $this->transactionRepository->getBankNames();
        $categories = $this->transactionDetailRepository->getCategories();
        $thirdNames = $this->transactionRepository->getThirdNames();

        return $this->render('accounting/transaction/edit.html.twig', [
            'bankNames' => $bankNames,
            'categories' => $categories,
            'form' => $form->createView(),
            'thirdNames' => $thirdNames,
            'transaction' => $transaction,
        ]);
    }
}
