<?php

declare(strict_types=1);

namespace App\Controller\Accounting\Transaction;

use App\Entity\Transaction;
use App\Form\TransactionAddType;
use App\Repository\TransactionRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

final class TransactionAddController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly TransactionRepository $transactionRepository,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route(
        '/accounting/transaction/add',
        name: 'app_accounting_transaction_add',
        methods: ['GET', 'POST']
    )]
    public function __invoke(Request $request): Response
    {
        $transaction = new Transaction();
        $transaction->setDateOperation(new DateTimeImmutable());

        $form = $this->createForm(TransactionAddType::class, $transaction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($transaction);
            $this->entityManager->flush();

            $this->addFlash('success', $this->translator->trans('notification.transaction_added'));

            return $this->redirectToRoute('app_accounting_transaction_edit', [
                'uuid' => (string) $transaction->getUuid(),
            ]);
        }

        $bankNames = $this->transactionRepository->getBankNames();
        $thirdNames = $this->transactionRepository->getThirdNames();

        return $this->render('accounting/transaction/add.html.twig', [
            'bankNames' => $bankNames,
            'form' => $form->createView(),
            'thirdNames' => $thirdNames,
        ]);
    }
}
