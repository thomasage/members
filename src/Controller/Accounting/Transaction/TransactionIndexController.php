<?php

declare(strict_types=1);

namespace App\Controller\Accounting\Transaction;

use App\Form\TransactionSearchType;
use App\Model\Pagination;
use App\Model\Search;
use App\Repository\TransactionDetailRepository;
use App\Repository\TransactionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class TransactionIndexController extends AbstractController
{
    public function __construct(
        private readonly TransactionDetailRepository $transactionDetailRepository,
        private readonly TransactionRepository $transactionRepository,
    ) {
    }

    #[Route(
        '/accounting/transaction',
        name: 'app_accounting_transaction_index',
        methods: ['GET']
    )]
    public function __invoke(Request $request): Response
    {
        $categories = $this->transactionDetailRepository->getCategories();
        $thirdNames = $this->transactionRepository->getThirdNames();

        $form = $this->createForm(TransactionSearchType::class, null, ['method' => 'GET']);
        $form->handleRequest($request);
        $search = new Search($request->query->getInt('page'));
        $search->setFilters($form->getData());
        $transactions = $this->transactionRepository->findBySearch($search);
        $pagination = new Pagination($transactions, $search, 'app_accounting_transaction_index');

        return $this->render('accounting/transaction/index.html.twig', [
            'categories' => $categories,
            'form' => $form->createView(),
            'pagination' => $pagination,
            'third_names' => $thirdNames,
            'transactions' => $transactions,
        ]);
    }
}
