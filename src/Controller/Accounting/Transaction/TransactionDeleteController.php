<?php

declare(strict_types=1);

namespace App\Controller\Accounting\Transaction;

use App\Form\TransactionDeleteType;
use App\Repository\TransactionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\Translation\TranslatorInterface;

final class TransactionDeleteController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly TransactionRepository $transactionRepository,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route(
        '/accounting/transaction/delete/{uuid}',
        name: 'app_accounting_transaction_delete',
        methods: ['GET', 'POST']
    )]
    public function __invoke(Request $request, string $uuid): Response
    {
        $transaction = $this->transactionRepository->findByUuid(Uuid::fromString($uuid));
        if (!$transaction) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(TransactionDeleteType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->remove($transaction);
            $this->entityManager->flush();

            $this->addFlash('success', $this->translator->trans('notification.transaction_deleted'));

            return $this->redirectToRoute('app_accounting_index');
        }

        return $this->render('accounting/transaction/delete.html.twig', [
            'form' => $form->createView(),
            'transaction' => $transaction,
        ]);
    }
}
