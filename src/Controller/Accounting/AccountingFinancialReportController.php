<?php

declare(strict_types=1);

namespace App\Controller\Accounting;

use App\Form\AccountingFinancialReportType;
use App\Repository\TransactionRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class AccountingFinancialReportController extends AbstractController
{
    public function __construct(private readonly TransactionRepository $transactionRepository)
    {
    }

    #[Route(
        '/accounting/report_financial',
        name: 'app_accounting_report_financial',
        methods: ['GET']
    )]
    public function __invoke(Request $request): Response
    {
        $form = $this->createForm(
            AccountingFinancialReportType::class,
            [
                'start' => (new DateTime('00:00:00'))->setDate((int) date('Y'), 1, 1),
                'end' => (new DateTime('00:00:00'))->setDate((int) date('Y'), 12, 31),
            ],
            ['method' => 'GET']
        );
        $form->handleRequest($request);

        $data = $form->getData();

        $report = $this->transactionRepository->getFinancialReport($data['start'] ?? null, $data['end'] ?? null);
        $total = [
            'credit' => array_sum(array_column($report, 'credit')),
            'debit' => array_sum(array_column($report, 'debit')),
        ];

        return $this->render(
            'accounting/report_financial.html.twig',
            [
                'form' => $form->createView(),
                'report' => $report,
                'total' => $total,
            ]
        );
    }
}
