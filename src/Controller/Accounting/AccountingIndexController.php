<?php

declare(strict_types=1);

namespace App\Controller\Accounting;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class AccountingIndexController extends AbstractController
{
    #[Route(
        '/accounting',
        name: 'app_accounting_index',
        methods: ['GET']
    )]
    public function __invoke(): Response
    {
        return $this->render('accounting/index.html.twig');
    }
}
