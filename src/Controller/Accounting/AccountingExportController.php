<?php

declare(strict_types=1);

namespace App\Controller\Accounting;

use App\Repository\TransactionDetailRepository;
use App\Repository\TransactionRepository;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Ods;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

final class AccountingExportController extends AbstractController
{
    public function __construct(
        private readonly TransactionDetailRepository $transactionDetailRepository,
        private readonly TransactionRepository $transactionRepository,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route(
        '/accounting/export',
        name: 'app_accounting_export',
        methods: ['GET']
    )]
    public function __invoke(Request $request): Response
    {
        if ($request->query->has('export')) {
            $response = new StreamedResponse(function () {
                $this->export();
            });
            $dispositionHeader = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                'Transactions.ods'
            );
            $response->headers->set('Content-Type', 'application/vnd.oasis.opendocument.spreadsheet; charset=utf-8');
            $response->headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            $response->headers->set('Content-Disposition', $dispositionHeader);

            return $response;
        }

        return $this->render('accounting/export.html.twig');
    }

    private function export(): void
    {
        $categories = $this->transactionDetailRepository->getCategories();
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', $this->translator->trans('field.date_operation'));
        $sheet->setCellValue('B1', $this->translator->trans('field.date_value'));
        $sheet->setCellValue('C1', $this->translator->trans('field.amount'));
        $sheet->setCellValue('D1', $this->translator->trans('field.third_name'));
        $sheet->setCellValue('E1', $this->translator->trans('field.payment_method'));
        $sheet->setCellValue('F1', $this->translator->trans('field.operation_number'));
        $sheet->setCellValue('G1', $this->translator->trans('field.bank_name'));
        $sheet->setCellValue('H1', $this->translator->trans('field.comment'));
        $colNum = 8;
        foreach ($categories as $category) {
            ++$colNum;
            $sheet->setCellValueByColumnAndRow($colNum, 1, $category);
        }
        $rowNum = 1;
        $transactions = $this->transactionRepository->findForExport();
        foreach ($transactions as $transaction) {
            ++$rowNum;
            $sheet->setCellValue("A$rowNum", $transaction->getDateOperation()->format('Y-m-d'));
            if ($transaction->getDateValue()) {
                $sheet->setCellValue("B$rowNum", $transaction->getDateValue()->format('Y-m-d'));
            }
            $sheet->setCellValue("C$rowNum", $transaction->getAmountInCents() / 100);
            $sheet->setCellValue("D$rowNum", $transaction->getThirdName());
            $sheet->setCellValue("E$rowNum", $transaction->getPaymentMethod()->getName());
            $sheet->setCellValue("F$rowNum", $transaction->getOperationNumber());
            $sheet->setCellValue("G$rowNum", $transaction->getBankName());
            $sheet->setCellValue("H$rowNum", $transaction->getComment());
            $colNum = 8;
            $totalByCategory = $transaction->getTotalByCategory();
            foreach ($categories as $category) {
                ++$colNum;
                if (isset($totalByCategory[$category])) {
                    $sheet->setCellValueByColumnAndRow($colNum, $rowNum, $totalByCategory[$category] / 100);
                }
            }
        }
        if ($rowNum > 1) {
            $sheet
                ->getStyle("A2:B$rowNum")
                ->getNumberFormat()
                ->setFormatCode(NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
            $sheet
                ->getStyle("C2:C$rowNum")
                ->getNumberFormat()
                ->setFormatCode(NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);
            if (($count = count($categories)) > 0) {
                $sheet
                    ->getStyleByColumnAndRow(9, 2, $count, $rowNum)
                    ->getNumberFormat()
                    ->setFormatCode(NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);
            }
        }
        $writer = new Ods($spreadsheet);
        $writer->save('php://output');
    }
}
