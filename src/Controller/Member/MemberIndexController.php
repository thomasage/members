<?php

declare(strict_types=1);

namespace App\Controller\Member;

use App\Form\MemberSearchType;
use App\Model\Pagination;
use App\Model\Search;
use App\Repository\MemberRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class MemberIndexController extends AbstractController
{
    public function __construct(private readonly MemberRepository $memberRepository)
    {
    }

    #[Route('/member', name: 'app_member_index', methods: ['GET'])]
    public function __invoke(Request $request): Response
    {
        $form = $this->createForm(MemberSearchType::class, null, ['method' => 'GET']);
        $form->handleRequest($request);
        $search = new Search($request->query->getInt('page'));
        $search->setFilters($form->getData());
        $members = $this->memberRepository->findBySearch($search);
        $pagination = new Pagination($members, $search, 'app_member_index');

        return $this->render('member/index.html.twig', [
            'form' => $form->createView(),
            'members' => $members,
            'pagination' => $pagination,
            'search' => $search,
        ]);
    }
}
