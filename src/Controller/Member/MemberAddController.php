<?php

declare(strict_types=1);

namespace App\Controller\Member;

use App\Entity\Member;
use App\Form\MemberType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

final class MemberAddController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route('/member/add', name: 'app_member_add', methods: ['GET', 'POST'])]
    public function __invoke(Request $request): Response
    {
        $member = new Member();

        $form = $this->createForm(MemberType::class, $member);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($member);
            $this->entityManager->flush();

            $this->addFlash('success', $this->translator->trans('notification.member_added'));

            return $this->redirectToRoute('app_member_index');
        }

        return $this->render('member/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
