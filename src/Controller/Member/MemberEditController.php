<?php

declare(strict_types=1);

namespace App\Controller\Member;

use App\Form\MemberType;
use App\Repository\MemberRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

final class MemberEditController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly MemberRepository $memberRepository,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route(
        '/member/edit/{uuid}',
        name: 'app_member_edit',
        methods: ['GET', 'POST']
    )]
    public function __invoke(Request $request, string $uuid): Response
    {
        $member = $this->memberRepository->findOneBy(['uuid' => $uuid]);
        if (!$member) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(MemberType::class, $member);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            $this->addFlash('success', $this->translator->trans('notification.member_updated'));

            return $this->redirectToRoute('app_member_show', [
                'uuid' => (string) $member->getUuid(),
            ]);
        }

        return $this->render('member/edit.html.twig', [
            'form' => $form->createView(),
            'member' => $member,
        ]);
    }
}
