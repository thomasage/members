<?php

declare(strict_types=1);

namespace App\Controller\Member;

use App\Repository\AttendanceRepository;
use App\Repository\MemberRepository;
use App\Repository\MembershipRepository;
use App\Repository\PromotionRepository;
use App\Repository\SeasonRepository;
use DateTimeImmutable;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

final class MemberShowController extends AbstractController
{
    public function __construct(
        private readonly AttendanceRepository $attendanceRepository,
        private readonly MemberRepository $memberRepository,
        private readonly MembershipRepository $membershipRepository,
        private readonly PromotionRepository $promotionRepository,
        private readonly SeasonRepository $seasonRepository,
    ) {
    }

    #[Route(
        '/member/show/{uuid}',
        name: 'app_member_show',
        methods: ['GET']
    )]
    public function __invoke(string $uuid): Response
    {
        $member = $this->memberRepository->findByUuid(Uuid::fromString($uuid));
        if (!$member) {
            throw new NotFoundHttpException();
        }

        $attendances = null;
        $season = $this->seasonRepository->findByDate(new DateTimeImmutable());
        if ($season) {
            try {
                $currentMembership = $this->membershipRepository->findByMemberAndSeason($member, $season);
                $attendances = $this->attendanceRepository->findByMembership($currentMembership);
            } catch (NonUniqueResultException $exception) {
            }
        }

        $memberships = $this->membershipRepository->findByMember($member);
        $promotions = $this->promotionRepository->findByMember($member);

        return $this->render('member/show.html.twig', [
            'attendances' => $attendances,
            'member' => $member,
            'memberships' => $memberships,
            'promotions' => $promotions,
        ]);
    }
}
