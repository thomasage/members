<?php

declare(strict_types=1);

namespace App\Controller\Season;

use App\Form\SeasonDeleteType;
use App\Repository\SeasonRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\Translation\TranslatorInterface;

final class SeasonDeleteController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly SeasonRepository $seasonRepository,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route(
        '/season/delete/{uuid}',
        name: 'app_season_delete',
        methods: ['GET', 'POST']
    )]
    public function __invoke(Request $request, string $uuid): Response
    {
        $season = $this->seasonRepository->findByUuid(Uuid::fromString($uuid));
        if (!$season) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(SeasonDeleteType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->remove($season);
            $this->entityManager->flush();

            $this->addFlash('success', $this->translator->trans('notification.season_deleted'));

            return $this->redirectToRoute('app_season_index');
        }

        return $this->render('season/delete.html.twig', [
            'form' => $form->createView(),
            'season' => $season,
        ]);
    }
}
