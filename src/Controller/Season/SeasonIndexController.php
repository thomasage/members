<?php

declare(strict_types=1);

namespace App\Controller\Season;

use App\Model\Search;
use App\Repository\SeasonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class SeasonIndexController extends AbstractController
{
    public function __construct(private readonly SeasonRepository $seasonRepository)
    {
    }

    #[Route(
        '/season',
        name: 'app_season_index',
        methods: ['GET']
    )]
    public function __invoke(Request $request): Response
    {
        $search = new Search($request->query->getInt('page'));
        $seasons = $this->seasonRepository->findBySearch($search);

        return $this->render('season/index.html.twig', [
            'seasons' => $seasons,
        ]);
    }
}
