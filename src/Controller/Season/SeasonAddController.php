<?php

declare(strict_types=1);

namespace App\Controller\Season;

use App\Entity\Season;
use App\Form\SeasonType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

final class SeasonAddController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route(
        '/season/add',
        name: 'app_season_add',
        methods: ['GET', 'POST']
    )]
    public function __invoke(Request $request): Response
    {
        $season = new Season();

        $form = $this->createForm(SeasonType::class, $season);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($season);
            $this->entityManager->flush();

            $this->addFlash('success', $this->translator->trans('notification.season_added'));

            return $this->redirectToRoute('app_season_index');
        }

        return $this->render('season/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
