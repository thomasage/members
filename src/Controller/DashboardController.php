<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\LessonRepository;
use App\Repository\MemberRepository;
use App\Repository\SeasonRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class DashboardController extends AbstractController
{
    public function __construct(
        private readonly LessonRepository $lessonRepository,
        private readonly MemberRepository $memberRepository,
        private readonly SeasonRepository $seasonRepository,
    ) {
    }

    #[Route('/', name: 'app_dashboard')]
    public function __invoke(): Response
    {
        $nextBirthdays = [];
        $season = $this->seasonRepository->findByDate(new DateTime());
        if ($season) {
            $nextBirthdays = $this->memberRepository->findNextBirthdays($season);
        }

        $lessonsWithoutAttendances = $this->lessonRepository->findWithoutAttendances(
            new DateTime('-3 weeks'),
            new DateTime()
        );

        return $this->render('dashboard/index.html.twig', [
            'lessons_without_attendances' => $lessonsWithoutAttendances,
            'nextBirthdays' => $nextBirthdays,
        ]);
    }
}
