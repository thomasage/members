<?php

declare(strict_types=1);

namespace App\Controller\Promotion;

use App\Entity\Promotion;
use App\Form\PromotionType;
use App\Repository\MemberRepository;
use App\Repository\PromotionRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\Translation\TranslatorInterface;

final class PromotionAddController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly MemberRepository $memberRepository,
        private readonly PromotionRepository $promotionRepository,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route(
        '/promotion/add/{memberUuid}',
        name: 'app_promotion_add',
        methods: ['GET', 'POST']
    )]
    public function __invoke(Request $request, string $memberUuid): Response
    {
        $member = $this->memberRepository->findByUuid(Uuid::fromString($memberUuid));
        if (!$member) {
            throw new NotFoundHttpException();
        }

        $promotion = new Promotion();
        $promotion
            ->setMember($member)
            ->setNominationDate(new DateTimeImmutable());

        $form = $this->createForm(PromotionType::class, $promotion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $found = $this->promotionRepository->findByMemberAndRank($member, $promotion->getRank());
            if ($found) {
                $this->addFlash('danger', $this->translator->trans('notification.promotion_already_exists'));
            } else {
                $this->entityManager->persist($promotion);
                $this->entityManager->flush();

                $this->addFlash('success', $this->translator->trans('notification.promotion_added'));

                return $this->redirectToRoute('app_member_show', [
                    'uuid' => $member->getUuid(),
                ]);
            }
        }

        return $this->render('promotion/add.html.twig', [
            'form' => $form->createView(),
            'member' => $member,
        ]);
    }
}
