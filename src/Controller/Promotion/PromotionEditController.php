<?php

declare(strict_types=1);

namespace App\Controller\Promotion;

use App\Form\PromotionType;
use App\Repository\PromotionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\Translation\TranslatorInterface;

final class PromotionEditController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly PromotionRepository $promotionRepository,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route(
        '/promotion/edit/{uuid}',
        name: 'app_promotion_edit',
        methods: ['GET', 'POST']
    )]
    public function __invoke(Request $request, string $uuid): Response
    {
        $promotion = $this->promotionRepository->findByUuid(Uuid::fromString($uuid));
        if (!$promotion) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(PromotionType::class, $promotion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            $this->addFlash('success', $this->translator->trans('notification.promotion_updated'));

            return $this->redirectToRoute('app_member_show', [
                'uuid' => $promotion->getMember()->getUuid(),
            ]);
        }

        return $this->render('promotion/edit.html.twig', [
            'form' => $form->createView(),
            'promotion' => $promotion,
        ]);
    }
}
