<?php

declare(strict_types=1);

namespace App\Controller\Promotion;

use App\Form\PromotionDeleteType;
use App\Repository\PromotionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\Translation\TranslatorInterface;

final class PromotionDeleteController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly PromotionRepository $promotionRepository,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route(
        '/promotion/delete/{uuid}',
        name: 'app_promotion_delete',
        methods: ['GET', 'POST']
    )]
    public function __invoke(Request $request, string $uuid): Response
    {
        $promotion = $this->promotionRepository->findByUuid(Uuid::fromString($uuid));
        if (!$promotion) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(PromotionDeleteType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $member = $promotion->getMember();
            $this->entityManager->remove($promotion);
            $this->entityManager->flush();

            $this->addFlash('success', $this->translator->trans('notification.promotion_deleted'));

            return $this->redirectToRoute('app_member_show', [
                'uuid' => $member->getUuid(),
            ]);
        }

        return $this->render('promotion/delete.html.twig', [
            'form' => $form->createView(),
            'promotion' => $promotion,
        ]);
    }
}
