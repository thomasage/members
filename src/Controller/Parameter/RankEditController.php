<?php

declare(strict_types=1);

namespace App\Controller\Parameter;

use App\Form\RankType;
use App\Repository\RankRepository;
use App\Tool\RankImageUploader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\Translation\TranslatorInterface;

final class RankEditController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly RankImageUploader $rankImageUploader,
        private readonly RankRepository $rankRepository,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route('/param/rank/edit/{uuid}', name: 'app_param_rank_edit', methods: ['GET', 'POST'])]
    public function __invoke(Request $request, string $uuid): Response
    {
        $rank = $this->rankRepository->findByUuid(Uuid::fromString($uuid));
        if (!$rank) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(RankType::class, $rank);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $image = $form->get('image')->getData();
            if ($image) {
                $this->rankImageUploader->upload($rank, $image);
            }
            $this->entityManager->flush();

            $this->addFlash('success', $this->translator->trans('notification.rank_updated'));

            return $this->redirectToRoute('app_param_rank_index');
        }

        return $this->render('param/rank/edit.html.twig', [
            'form' => $form->createView(),
            'rank' => $rank,
        ]);
    }
}
