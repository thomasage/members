<?php

declare(strict_types=1);

namespace App\Controller\Parameter;

use App\Entity\Rank;
use App\Form\RankType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

final class RankAddController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly TranslatorInterface $translator,
    ) {
    }

    #[Route('/param/rank/add', name: 'app_param_rank_add', methods: ['GET', 'POST'])]
    public function __invoke(Request $request): Response
    {
        $rank = new Rank();

        $form = $this->createForm(RankType::class, $rank);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($rank);
            $this->entityManager->flush();

            $this->addFlash('success', $this->translator->trans('notification.rank_added'));

            return $this->redirectToRoute('app_param_rank_index');
        }

        return $this->render('param/rank/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
