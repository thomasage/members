<?php

declare(strict_types=1);

namespace App\Controller\Parameter;

use App\Repository\RankRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class RankIndexController extends AbstractController
{
    public function __construct(private readonly RankRepository $rankRepository)
    {
    }

    #[Route('/param/rank', name: 'app_param_rank_index', methods: ['GET'])]
    public function __invoke(): Response
    {
        $ranks = $this->rankRepository->findBy([], ['position' => 'ASC']);

        return $this->render(
            'param/rank/index.html.twig',
            [
                'ranks' => $ranks,
            ]
        );
    }
}
