<?php

declare(strict_types=1);

namespace App\Calendar;

use DateInterval;
use DatePeriod;
use DateTimeImmutable;
use Generator;

final class Calendar
{
    public function __construct(private readonly DateTimeImmutable $start, private readonly DateTimeImmutable $end)
    {
    }

    /**
     * @return Generator<Month>
     */
    public function getMonths(): Generator
    {
        $start = $this->start->setDate((int) $this->start->format('Y'), (int) $this->start->format('n'), 1);
        $end = $this->end->setDate((int) $this->end->format('Y'), (int) $this->end->format('n') + 1, 0);
        $period = new DatePeriod($start, new DateInterval('P1M'), $end);
        foreach ($period as $item) {
            yield new Month((int) $item->format('Y'), (int) $item->format('n'));
        }
    }
}
