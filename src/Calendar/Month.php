<?php

declare(strict_types=1);

namespace App\Calendar;

use DateInterval;
use DatePeriod;
use DateTimeImmutable;
use Generator;
use RuntimeException;

final class Month
{
    public function __construct(private readonly int $year, private readonly int $month)
    {
    }

    public function getFirstDay(): DateTimeImmutable
    {
        $timestamp = mktime(0, 0, 0, $this->month, 1, $this->year);
        if (!$timestamp) {
            throw new RuntimeException('Unable to get timestamp');
        }

        return (new DateTimeImmutable())->setTimestamp($timestamp);
    }

    /**
     * @return Generator<Week>
     */
    public function getWeeks(): Generator
    {
        $firstDayOfMonth = mktime(0, 0, 0, $this->month, 1, $this->year);
        if (!$firstDayOfMonth) {
            throw new RuntimeException('Unable to get timestamp');
        }
        $firstDayToDisplay = strtotime('monday this week', $firstDayOfMonth);
        $firstDayToDisplay = (new DateTimeImmutable())->setTimestamp($firstDayToDisplay);
        $lastDayOfMonth = mktime(0, 0, 0, $this->month + 1, 0, $this->year);
        if (!$lastDayOfMonth) {
            throw new RuntimeException('Unable to get timestamp');
        }
        $lastDayToDisplay = strtotime('sunday this week', $lastDayOfMonth);
        $lastDayToDisplay = (new DateTimeImmutable())->setTimestamp($lastDayToDisplay);
        $period = new DatePeriod($firstDayToDisplay, new DateInterval('P1W'), $lastDayToDisplay);
        foreach ($period as $item) {
            yield new Week((int) $item->format('Y'), $this->month, (int) $item->format('W'));
        }
    }
}
