<?php

declare(strict_types=1);

namespace App\Calendar;

use DateTimeImmutable;
use DateTimeInterface;
use Exception;

final class Day
{
    public function __construct(private readonly DateTimeImmutable $date)
    {
    }

    public function equals(string|DateTimeInterface $date): bool
    {
        if (is_string($date)) {
            try {
                $date = new DateTimeImmutable($date);
            } catch (Exception $exception) {
                return false;
            }
        }

        return $date->format('Y-m-d') === $this->date->format('Y-m-d');
    }

    public function getDate(): DateTimeImmutable
    {
        return $this->date;
    }
}
