<?php

declare(strict_types=1);

namespace App\Calendar;

use DateInterval;
use DatePeriod;
use DateTimeImmutable;
use Generator;
use RuntimeException;

final class Week
{
    public function __construct(private readonly int $year, private readonly int $month, private readonly int $week)
    {
    }

    /**
     * @return Generator<Day|null>
     */
    public function getDays(): Generator
    {
        $timestamp = strtotime($this->year.'W'.str_pad((string) $this->week, 2, '0', STR_PAD_LEFT));
        if (!$timestamp) {
            throw new RuntimeException('Unable to compute timestamp');
        }
        $start = self::createFromTimestamp(strtotime('monday', $timestamp));
        $end = self::createFromTimestamp(strtotime('sunday +1 day', $timestamp));
        $period = new DatePeriod($start, new DateInterval('P1D'), $end);
        foreach ($period as $item) {
            if ((int) $item->format('n') === $this->month) {
                yield new Day(DateTimeImmutable::createFromInterface($item));
            } else {
                yield null;
            }
        }
    }

    private static function createFromTimestamp(int $timestamp): DateTimeImmutable
    {
        return (new DateTimeImmutable())->setTimestamp($timestamp);
    }
}
