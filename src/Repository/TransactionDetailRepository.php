<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\TransactionDetail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class TransactionDetailRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TransactionDetail::class);
    }

    /**
     * @return string[]
     */
    public function getCategories(): array
    {
        return $this
            ->createQueryBuilder('transaction_detail')
            ->select('transaction_detail.category')
            ->distinct()
            ->andWhere('transaction_detail.category IS NOT NULL')
            ->addOrderBy('transaction_detail.category', 'ASC')
            ->getQuery()
            ->getSingleColumnResult();
    }
}
