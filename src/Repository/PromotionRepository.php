<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Member;
use App\Entity\Promotion;
use App\Entity\Rank;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Uid\Uuid;

final class PromotionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Promotion::class);
    }

    /**
     * @return Promotion[]
     */
    public function findByMember(Member $member): array
    {
        return $this->createQueryBuilder('promotion')
            ->innerJoin('promotion.rank', 'rank')
            ->addSelect('rank')
            ->andWhere('promotion.member = :member')
            ->setParameter('member', $member)
            ->orderBy('promotion.nominationDate', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findByMemberAndRank(Member $member, Rank $rank): ?Promotion
    {
        try {
            return $this
                ->createQueryBuilder('promotion')
                ->andWhere('promotion.member = :member')
                ->andWhere('promotion.rank = :rank')
                ->setParameter('member', $member)
                ->setParameter('rank', $rank)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findByUuid(Uuid $uuid): ?Promotion
    {
        try {
            return $this
                ->createQueryBuilder('promotion')
                ->innerJoin('promotion.member', 'm')
                ->addSelect('m')
                ->andWhere('promotion.uuid = :uuid')
                ->setParameter('uuid', $uuid->toBinary())
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
