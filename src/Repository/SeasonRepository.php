<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Season;
use App\Model\Search;
use DateTimeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Uid\Uuid;

final class SeasonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Season::class);
    }

    public function findByDate(DateTimeInterface $date): ?Season
    {
        try {
            return $this
                ->createQueryBuilder('season')
                ->andWhere(':date BETWEEN season.start AND season.end')
                ->setParameter('date', $date->format('Y-m-d'))
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findBySearch(Search $search): Paginator
    {
        $builder = $this
            ->createQueryBuilder('season')
            ->addOrderBy('season.start', 'DESC')
            ->setFirstResult($search->getPage() * $search->getMaxResultsPerPage())
            ->setMaxResults($search->getMaxResultsPerPage());

        return new Paginator($builder);
    }

    public function findByUuid(Uuid $uuid): ?Season
    {
        return $this->findOneBy(['uuid' => $uuid]);
    }

    public function findLatest(): ?Season
    {
        try {
            return $this
                ->createQueryBuilder('season')
                ->addOrderBy('season.start', 'DESC')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
