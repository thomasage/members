<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Lesson;
use App\Entity\Member;
use App\Entity\Season;
use App\Model\Search;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Uid\Uuid;

final class MemberRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Member::class);
    }

    /**
     * @return Member[]
     */
    public function findAvailableByLesson(Lesson $lesson): array
    {
        return $this
            ->createQueryBuilder('m')
            ->innerJoin('m.memberships', 'memberships')
            ->innerJoin('memberships.season', 'season')
            ->andWhere(':date BETWEEN season.start AND season.end')
            ->andWhere('memberships.level IN ( :levels )')
            ->setParameter('date', $lesson->getStartAt())
            ->setParameter('levels', $lesson->getLevels())
            ->addOrderBy('m.firstName', 'ASC')
            ->addOrderBy('m.lastName', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findBySearch(Search $search): Paginator
    {
        $builder = $this
            ->createQueryBuilder('m')
            ->addOrderBy('m.firstName', 'ASC')
            ->addOrderBy('m.lastName', 'ASC')
            ->setFirstResult($search->getPage() * $search->getMaxResultsPerPage())
            ->setMaxResults($search->getMaxResultsPerPage());

        if ($firstName = $search->getFilter('firstName')) {
            $builder
                ->andWhere('m.firstName LIKE :firstName')
                ->setParameter('firstName', "%$firstName%");
        }

        if ($lastName = $search->getFilter('lastName')) {
            $builder
                ->andWhere('m.lastName LIKE :lastName')
                ->setParameter('lastName', "%$lastName%");
        }

        if ($season = $search->getFilter('season')) {
            $builder
                ->innerJoin('m.memberships', 'memberships')
                ->innerJoin('memberships.season', 'season')
                ->andWhere('season.uuid = :season')
                ->setParameter('season', Uuid::fromString($season)->toBinary());
        }

        return new Paginator($builder);
    }

    /**
     * @return Member[]
     */
    public function findNextBirthdays(Season $season): array
    {
        $start = new DateTime('-2 weeks');
        $end = new DateTime('+4 weeks');

        $builder = $this
            ->createQueryBuilder('m')
            ->innerJoin('m.memberships', 'memberships')
            ->andWhere('memberships.season = :season')
            ->andWhere('SUBSTRING( m.birthday, 6 ) BETWEEN :start AND :stop')
            ->setParameter('season', $season)
            ->setParameter('start', $start->format('m-d'))
            ->setParameter('stop', $end->format('m-d'))
            ->addOrderBy('SUBSTRING( m.birthday, 6 )', 'ASC');

        return $builder->getQuery()->getResult();
    }

    public function findByUuid(Uuid $uuid): ?Member
    {
        try {
            return $this
                ->createQueryBuilder('m')
                ->andWhere('m.uuid = :uuid')
                ->setParameter('uuid', $uuid->toBinary())
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @return array<array{city:string,total:int}>
     */
    public function findStatisticsCities(Season $season): array
    {
        $sql = <<<SQL
SELECT member.city, COUNT( member.id ) AS total
FROM member
INNER JOIN membership ON member.id = membership.member_id
INNER JOIN season ON membership.season_id = season.id
WHERE season.id = :season
GROUP BY member.city
ORDER BY total DESC
SQL;

        $resultSetMapping = new ResultSetMapping();
        $resultSetMapping->addScalarResult('city', 'city');
        $resultSetMapping->addScalarResult('total', 'total', 'integer');
        $query = $this->_em->createNativeQuery($sql, $resultSetMapping);
        $query->setParameter('season', $season->getId());

        return $query->getArrayResult();
    }
}
