<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Transaction;
use App\Model\Search;
use DateTimeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Uid\Uuid;

final class TransactionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Transaction::class);
    }

    public function findBySearch(Search $search): Paginator
    {
        $builder = $this
            ->createQueryBuilder('transaction')
            ->innerJoin('transaction.paymentMethod', 'paymentMethod')
            ->leftJoin('transaction.details', 'details')
            ->addSelect('paymentMethod')
            ->addOrderBy('transaction.dateOperation', 'DESC')
            ->addOrderBy('transaction.dateValue', 'DESC')
            ->setFirstResult($search->getPage() * $search->getMaxResultsPerPage())
            ->setMaxResults($search->getMaxResultsPerPage());

        if ($category = $search->getFilter('category')) {
            $builder
                ->andWhere('details.category LIKE :category')
                ->setParameter('category', "%$category%");
        }

        if ($thirdName = $search->getFilter('thirdName')) {
            $builder
                ->andWhere('transaction.thirdName LIKE :thirdName')
                ->setParameter('thirdName', "%$thirdName%");
        }

        return new Paginator($builder->getQuery());
    }

    public function findByUuid(Uuid $uuid): ?Transaction
    {
        try {
            return $this
                ->createQueryBuilder('transaction')
                ->leftJoin('transaction.details', 'details')
                ->addSelect('details')
                ->andWhere('transaction.uuid = :uuid')
                ->setParameter('uuid', $uuid->toBinary())
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @return Transaction[]
     */
    public function findForExport(): array
    {
        return $this
            ->createQueryBuilder('transaction')
            ->innerJoin('transaction.paymentMethod', 'payment_method')
            ->leftJoin('transaction.details', 'details')
            ->addSelect('payment_method')
            ->addSelect('details')
            ->addOrderBy('transaction.dateOperation', 'ASC')
            ->addOrderBy('transaction.dateValue', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return string[]
     */
    public function getBankNames(): array
    {
        return $this
            ->createQueryBuilder('transaction')
            ->select('transaction.bankName')
            ->distinct()
            ->andWhere('transaction.bankName IS NOT NULL')
            ->addOrderBy('transaction.bankName', 'ASC')
            ->getQuery()
            ->getSingleColumnResult();
    }

    /**
     * @return string[]
     */
    public function getThirdNames(): array
    {
        return $this
            ->createQueryBuilder('transaction')
            ->select('transaction.thirdName')
            ->distinct()
            ->andWhere('transaction.thirdName IS NOT NULL')
            ->addOrderBy('transaction.thirdName', 'ASC')
            ->getQuery()
            ->getSingleColumnResult();
    }

    /**
     * @return array<array{category:string,credit:int,debit:int}>
     */
    public function getFinancialReport(?DateTimeInterface $start = null, ?DateTimeInterface $end = null): array
    {
        $sql = <<<SQL
SELECT transaction_detail.category,
       SUM( IF( transaction_detail.amount_in_cents > 0, transaction_detail.amount_in_cents, 0 ) ) AS credit,
       SUM( IF( transaction_detail.amount_in_cents < 0, transaction_detail.amount_in_cents, 0 ) ) AS debit
FROM transaction
INNER JOIN transaction_detail ON transaction.id = transaction_detail.transaction_id
WHERE 1
SQL;
        if ($start) {
            $sql .= ' AND transaction.date_value >= :start';
        }
        if ($end) {
            $sql .= ' AND transaction.date_value <= :end';
        }
        $sql .= ' GROUP BY transaction_detail.category ORDER BY transaction_detail.category';

        $resultSetMapping = new ResultSetMapping();
        $resultSetMapping->addScalarResult('category', 'category');
        $resultSetMapping->addScalarResult('credit', 'credit', 'integer');
        $resultSetMapping->addScalarResult('debit', 'debit', 'integer');

        $query = $this->_em->createNativeQuery($sql, $resultSetMapping);
        if ($start) {
            $query->setParameter('start', $start->format('Y-m-d'));
        }
        if ($end) {
            $query->setParameter('end', $end->format('Y-m-d'));
        }

        return $query->getArrayResult();
    }
}
