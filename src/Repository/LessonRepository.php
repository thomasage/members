<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Lesson;
use App\Model\Search;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Uid\Uuid;

final class LessonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lesson::class);
    }

    /**
     * @return Lesson[]
     */
    public function findActiveByDate(DateTimeInterface $date): array
    {
        return $this
            ->createQueryBuilder('lesson')
            ->andWhere('lesson.active = 1')
            ->andWhere('SUBSTRING( lesson.startAt, 1, 10 ) = :date')
            ->setParameter('date', $date->format('Y-m-d'))
            ->addOrderBy('lesson.startAt', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findByUuid(Uuid $uuid): ?Lesson
    {
        try {
            return $this
                ->createQueryBuilder('lesson')
                ->leftJoin('lesson.levels', 'levels')
                ->leftJoin('lesson.themes', 'themes')
                ->leftJoin('lesson.attendances', 'attendances')
                ->leftJoin('attendances.member', 'm')
                ->addSelect('attendances')
                ->addSelect('m')
                ->addSelect('levels')
                ->addSelect('themes')
                ->andWhere('lesson.uuid = :uuid')
                ->setParameter('uuid', $uuid->toBinary())
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @return string[]
     */
    public function findDaysWithAtLeastOneLesson(DateTimeImmutable $start, DateTimeImmutable $end): array
    {
        return $this
            ->createQueryBuilder('lesson')
            ->select('SUBSTRING( lesson.startAt, 1, 10 )')
            ->distinct()
            ->andWhere('lesson.active = 1')
            ->andWhere('SUBSTRING( lesson.startAt, 1, 10 ) BETWEEN :start AND :end')
            ->setParameter('start', $start->format('Y-m-d'))
            ->setParameter('end', $end->format('Y-m-d'))
            ->getQuery()
            ->getSingleColumnResult();
    }

    public function findBySearch(Search $search): Paginator
    {
        $builder = $this
            ->createQueryBuilder('lesson')
            ->leftJoin('lesson.attendances', 'attendances')
            ->leftJoin('lesson.levels', 'levels')
            ->leftJoin('lesson.themes', 'themes')
            ->addSelect('attendances')
            ->addSelect('levels')
            ->addSelect('themes')
            ->addOrderBy('lesson.startAt', 'DESC')
            ->setFirstResult($search->getPage() * $search->getMaxResultsPerPage())
            ->setMaxResults($search->getMaxResultsPerPage());

        if ($level = $search->getFilter('level')) {
            $builder
                ->andWhere('levels.uuid = :level')
                ->setParameter('level', Uuid::fromString($level)->toBinary());
        }

        return new Paginator($builder);
    }

    /**
     * @return Lesson[]
     */
    public function findStatisticsThemes(DateTimeInterface $start, DateTimeInterface $end, Uuid $levelUuid): array
    {
        return $this
            ->createQueryBuilder('lesson')
            ->innerJoin('lesson.levels', 'levels')
            ->innerJoin('lesson.themes', 'themes')
            ->addSelect('themes')
            ->andWhere('lesson.startAt BETWEEN :start AND :end')
            ->andWhere('levels.uuid = :level')
            ->setParameter('start', $start->format('Y-m-d').' 00:00:00')
            ->setParameter('end', $end->format('Y-m-d').' 23:59:59')
            ->setParameter('level', $levelUuid->toBinary())
            ->addOrderBy('lesson.startAt', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Lesson[]
     */
    public function findWithoutAttendances(DateTimeInterface $start, DateTimeInterface $end): array
    {
        return $this
            ->createQueryBuilder('lesson')
            ->leftJoin('lesson.levels', 'levels')
            ->leftJoin('lesson.attendances', 'attendances')
            ->addSelect('levels')
            ->andWhere('lesson.active = 1')
            ->andWhere('attendances.id IS NULL')
            ->andWhere('lesson.startAt BETWEEN :start AND :end')
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->addOrderBy('lesson.startAt', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
