<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Member;
use App\Entity\Membership;
use App\Entity\Season;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Uid\Uuid;

final class MembershipRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Membership::class);
    }

    /**
     * @return Membership[]
     */
    public function findByMember(Member $member): array
    {
        return $this
            ->createQueryBuilder('membership')
            ->innerJoin('membership.season', 'season')
            ->addSelect('season')
            ->andWhere('membership.member = :member')
            ->setParameter('member', $member)
            ->orderBy('season.start', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findByUuid(Uuid $uuid): ?Membership
    {
        try {
            return $this
                ->createQueryBuilder('membership')
                ->innerJoin('membership.member', 'm')
                ->addSelect('m')
                ->andWhere('membership.uuid = :uuid')
                ->setParameter('uuid', $uuid->toBinary())
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @return array<array{level_name:string,season_start:int,total:int}>
     */
    public function findStatisticsEvolution(): array
    {
        $sql = <<<SQL
SELECT YEAR( season.start ) AS season_start,
       level.name AS level_name,
       COUNT( membership.id ) AS total
FROM membership
INNER JOIN level ON membership.level_id = level.id
INNER JOIN season ON membership.season_id = season.id
GROUP BY season.start, level.id
ORDER BY season.start
SQL;

        $resultSetMapping = new ResultSetMapping();
        $resultSetMapping->addScalarResult('level_name', 'level_name');
        $resultSetMapping->addScalarResult('season_start', 'season_start', 'integer');
        $resultSetMapping->addScalarResult('total', 'total', 'integer');

        return $this->_em->createNativeQuery($sql, $resultSetMapping)->getArrayResult();
    }

    /**
     * @return array<array{age_bracket:int,gender:string,total:int}>
     */
    public function statisticsNumber(Uuid $seasonUuid): array
    {
        $sql = <<<SQL
SELECT member.gender,
       CASE
           WHEN member.birthday > NOW( ) - INTERVAL 12 YEAR THEN 0
           WHEN member.birthday > NOW( ) - INTERVAL 18 YEAR THEN 1
           ELSE 2
       END AS age_bracket,
       COUNT( membership.id ) AS total
FROM member
INNER JOIN membership ON member.id = membership.member_id
INNER JOIN season ON membership.season_id = season.id
WHERE season.uuid = :season
GROUP BY member.gender, age_bracket
ORDER BY age_bracket, member.gender
SQL;

        $resultSetMapping = new ResultSetMapping();
        $resultSetMapping->addScalarResult('age_bracket', 'age_bracket', 'integer');
        $resultSetMapping->addScalarResult('gender', 'gender');
        $resultSetMapping->addScalarResult('total', 'total', 'integer');

        $query = $this->_em->createNativeQuery($sql, $resultSetMapping);
        $query->setParameter('season', $seasonUuid->toBinary());

        return $query->getArrayResult();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findByMemberAndSeason(Member $member, Season $season): Membership
    {
        return $this
            ->createQueryBuilder('membership')
            ->andWhere('membership.member = :member')
            ->andWhere('membership.season = :season')
            ->setParameter('member', $member)
            ->setParameter('season', $season)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
