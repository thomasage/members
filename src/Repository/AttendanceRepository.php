<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Attendance;
use App\Entity\Membership;
use App\Entity\Season;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Uid\Uuid;

final class AttendanceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Attendance::class);
    }

    public function findByUuid(Uuid $uuid): ?Attendance
    {
        try {
            return $this
                ->createQueryBuilder('attendance')
                ->andWhere('attendance.uuid = :uuid')
                ->setParameter('uuid', $uuid->toBinary())
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @return array<array{start_at:string,state:integer}>
     */
    public function findByMembership(Membership $membership): array
    {
        /** @var Membership $level */
        $level = $membership->getLevel();
        /** @var Season $season */
        $season = $membership->getSeason();

        $resultSetMapping = new ResultSetMapping();
        $resultSetMapping->addScalarResult('start_at', 'start_at');
        $resultSetMapping->addScalarResult('state', 'state', 'integer');

        $sql = <<<'SQL'
SELECT lesson.start_at,
       IFNULL( attendance.state, 0 ) AS state
FROM lesson
INNER JOIN lesson_level ON lesson.id = lesson_level.lesson_id
INNER JOIN level ON lesson_level.level_id = level.id
LEFT JOIN attendance ON lesson.id = attendance.lesson_id AND attendance.member_id = :member 
WHERE lesson.start_at BETWEEN :start AND :end
AND lesson.active = 1
AND level.id = :level
ORDER BY lesson.start_at
SQL;
        $query = $this->_em->createNativeQuery($sql, $resultSetMapping);
        $query->setParameter('end', $season->getEnd()->format('Y-m-d 23:59:59'));
        $query->setParameter('level', $level->getId());
        $query->setParameter('member', $membership->getMember()->getId());
        $query->setParameter('start', $season->getStart()->format('Y-m-d 00:00:00'));

        return $query->getArrayResult();
    }
}
