<?php

declare(strict_types=1);

namespace App\Form;

use App\Repository\SeasonRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class ChoiceSeasonType extends AbstractType
{
    public function __construct(private readonly SeasonRepository $seasonRepository)
    {
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $seasons = $this->seasonRepository->findBy([], ['start' => 'DESC']);
        $choices = [];
        foreach ($seasons as $season) {
            $choices[(string) $season] = (string) $season->getUuid();
        }
        $resolver->setDefaults(['choices' => $choices]);
    }

    public function getParent(): string
    {
        return ChoiceType::class;
    }
}
