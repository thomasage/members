<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Rank;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Range;

final class RankType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'attr' => [
                        'autofocus' => true,
                    ],
                    'constraints' => [
                        new Length(min: 1, max: 100),
                    ],
                    'label' => 'field.name',
                    'required' => true,
                ]
            )
            ->add(
                'description',
                TextType::class,
                [
                    'constraints' => [
                        new Length(min: 1, max: 100),
                    ],
                    'label' => 'field.description',
                    'required' => true,
                ]
            )
            ->add(
                'position',
                IntegerType::class,
                [
                    'constraints' => [
                        new Range(min: 0),
                    ],
                    'label' => 'field.position',
                    'required' => true,
                ]
            )
            ->add(
                'lessons',
                IntegerType::class,
                [
                    'constraints' => [
                        new Range(min: 1),
                    ],
                    'label' => 'field.lessons',
                    'required' => false,
                ]
            )
            ->add(
                'ageMin',
                IntegerType::class,
                [
                    'constraints' => [
                        new Range(min: 1),
                    ],
                    'label' => 'field.age_min',
                    'required' => true,
                ]
            )
            ->add(
                'ageMax',
                IntegerType::class,
                [
                    'constraints' => [
                        new Range(min: 1),
                    ],
                    'label' => 'field.age_max',
                    'required' => false,
                ]
            )
            ->addEventListener(FormEvents::PRE_SET_DATA, [$this, 'addFieldImage']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Rank::class,
        ]);
    }

    public function addFieldImage(FormEvent $event): void
    {
        /** @var Rank $rank */
        $rank = $event->getData();
        if (null === $rank->getId()) {
            return;
        }
        $event
            ->getForm()
            ->add(
                'image',
                FileType::class,
                [
                    'attr' => [
                        'accept' => 'image/png',
                    ],
                    'constraints' => [
                        new File([
                            'maxSize' => '10k',
                            'mimeTypes' => [
                                'image/png',
                            ],
                            'mimeTypesMessage' => 'Please upload a valid PNG image',
                        ]),
                    ],
                    'label' => 'field.image',
                    'mapped' => false,
                    'required' => false,
                ]
            );
    }
}
