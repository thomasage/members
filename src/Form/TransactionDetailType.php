<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\TransactionDetail;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;

final class TransactionDetailType extends AbstractType
{
    private string $currency;

    public function __construct(string $currency)
    {
        $this->currency = $currency;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'category',
                TextType::class,
                [
                    'attr' => [
                        'list' => 'categories',
                    ],
                    'constraints' => [
                        new Length(min: 1, max: 100),
                        new NotNull(),
                    ],
                    'label' => 'field.category',
                    'required' => true,
                ]
            )
            ->add(
                'amountInCents',
                MoneyType::class,
                [
                    'constraints' => [
                        new NotNull(),
                    ],
                    'currency' => $this->currency,
                    'divisor' => 100,
                    'label' => 'field.amount',
                    'required' => true,
                ]
            )
            ->add(
                'comment',
                TextType::class,
                [
                    'constraints' => [
                        new Length(min: 1, max: 255),
                    ],
                    'label' => 'field.comment',
                    'required' => false,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TransactionDetail::class,
        ]);
    }
}
