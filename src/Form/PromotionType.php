<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Promotion;
use App\Entity\Rank;
use App\Repository\RankRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

final class PromotionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'member',
                TextType::class,
                [
                    'disabled' => true,
                    'label' => 'field.member',
                ]
            )
            ->add(
                'rank',
                EntityType::class,
                [
                    'class' => Rank::class,
                    'constraints' => [
                        new NotNull(),
                    ],
                    'label' => 'field.rank',
                    'query_builder' => static function (RankRepository $rankRepository): QueryBuilder {
                        return $rankRepository
                            ->createQueryBuilder('rank')
                            ->addOrderBy('rank.position', 'ASC');
                    },
                    'required' => true,
                ]
            )
            ->add(
                'nominationDate',
                DateType::class,
                [
                    'constraints' => [
                        new NotNull(),
                    ],
                    'label' => 'field.nomination_date',
                    'required' => true,
                    'widget' => 'single_text',
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Promotion::class,
        ]);
    }
}
