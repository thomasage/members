<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;

final class LessonDateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'lessons',
                CollectionType::class,
                [
                    'entry_options' => [
                        'label' => false,
                    ],
                    'entry_type' => LessonAttendanceType::class,
                    'label' => false,
                ]
            );
    }
}
