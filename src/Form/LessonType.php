<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Lesson;
use App\Entity\Level;
use App\Repository\LevelRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Range;

final class LessonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'startAt',
                DateTimeType::class,
                [
                    'input' => 'datetime_immutable',
                    'label' => 'field.start',
                    'required' => true,
                    'widget' => 'single_text',
                ]
            )
            ->add(
                'durationInMinutes',
                NumberType::class,
                [
                    'constraints' => [
                        new NotNull(),
                        new Range(min: 1),
                    ],
                    'label' => 'field.duration_in_minutes',
                    'required' => true,
                ]
            )
            ->add(
                'levels',
                EntityType::class,
                [
                    'class' => Level::class,
                    'expanded' => true,
                    'label' => 'field.levels',
                    'multiple' => true,
                    'query_builder' => static function (LevelRepository $levelRepository): QueryBuilder {
                        return $levelRepository->createQueryBuilder('level')->addOrderBy('level.name', 'ASc');
                    },
                    'required' => false,
                ]
            )
            ->add(
                'comment',
                TextType::class,
                [
                    'constraints' => [
                        new Length(min: 1, max: 255),
                    ],
                    'label' => 'field.comment',
                    'required' => false,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Lesson::class,
        ]);
    }
}
