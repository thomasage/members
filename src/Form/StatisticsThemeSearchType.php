<?php

declare(strict_types=1);

namespace App\Form;

use App\Repository\LevelRepository;
use App\Repository\SeasonRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class StatisticsThemeSearchType extends AbstractType
{
    private LevelRepository $levelRepository;
    private SeasonRepository $seasonRepository;

    public function __construct(LevelRepository $levelRepository, SeasonRepository $seasonRepository)
    {
        $this->levelRepository = $levelRepository;
        $this->seasonRepository = $seasonRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $levels = $seasons = [];
        foreach ($this->levelRepository->findBy([], ['name' => 'ASC']) as $level) {
            $levels[(string) $level] = (string) $level->getUuid();
        }
        foreach ($this->seasonRepository->findBy([], ['start' => 'DESC']) as $season) {
            $seasons[(string) $season] = (string) $season->getUuid();
        }

        $builder
            ->add(
                'season',
                ChoiceType::class,
                [
                    'choices' => $seasons,
                    'expanded' => false,
                    'label' => 'field.season',
                    'multiple' => false,
                    'required' => true,
                ]
            )
            ->add(
                'level',
                ChoiceType::class,
                [
                    'choices' => $levels,
                    'expanded' => false,
                    'label' => 'field.level',
                    'multiple' => false,
                    'required' => true,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return '';
    }
}
