<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Member;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

final class MemberType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'firstName',
                TextType::class,
                [
                    'attr' => [
                        'autofocus' => true,
                    ],
                    'constraints' => [
                        new Length(min: 1, max: 100),
                    ],
                    'label' => 'field.first_name',
                    'required' => true,
                ]
            )
            ->add(
                'lastName',
                TextType::class,
                [
                    'constraints' => [
                        new Length(min: 1, max: 100),
                    ],
                    'label' => 'field.last_name',
                    'required' => true,
                ]
            )
            ->add(
                'gender',
                ChoiceType::class,
                [
                    'choices' => [
                        'gender.m' => 'm',
                        'gender.f' => 'f',
                    ],
                    'label' => 'field.gender',
                    'required' => true,
                ]
            )
            ->add(
                'birthday',
                BirthdayType::class,
                [
                    'input' => 'datetime_immutable',
                    'label' => 'field.birthday',
                    'required' => true,
                    'widget' => 'single_text',
                ]
            )
            ->add(
                'streetAddress',
                TextType::class,
                [
                    'constraints' => [
                        new Length(min: 1, max: 100),
                    ],
                    'label' => 'field.street_address',
                    'required' => true,
                ]
            )
            ->add(
                'postCode',
                TextType::class,
                [
                    'constraints' => [
                        new Length(min: 1, max: 10),
                    ],
                    'label' => 'field.post_code',
                    'required' => true,
                ]
            )
            ->add(
                'city',
                TextType::class,
                [
                    'constraints' => [
                        new Length(min: 1, max: 100),
                    ],
                    'label' => 'field.city',
                    'required' => true,
                ]
            )
            ->add(
                'phone0',
                TextType::class,
                [
                    'attr' => [
                        'maxlength' => 20,
                    ],
                    'constraints' => [
                        new Length(max: 20),
                    ],
                    'label' => 'field.phone0',
                    'required' => false,
                ]
            )
            ->add(
                'phone1',
                TextType::class,
                [
                    'attr' => [
                        'maxlength' => 20,
                    ],
                    'constraints' => [
                        new Length(max: 20),
                    ],
                    'label' => 'field.phone1',
                    'required' => false,
                ]
            )
            ->add(
                'phone2',
                TextType::class,
                [
                    'attr' => [
                        'maxlength' => 20,
                    ],
                    'constraints' => [
                        new Length(max: 20),
                    ],
                    'label' => 'field.phone2',
                    'required' => false,
                ]
            )
            ->add(
                'phone3',
                TextType::class,
                [
                    'attr' => [
                        'maxlength' => 20,
                    ],
                    'constraints' => [
                        new Length(max: 20),
                    ],
                    'label' => 'field.phone3',
                    'required' => false,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Member::class,
        ]);
    }
}
