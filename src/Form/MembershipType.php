<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Level;
use App\Entity\Membership;
use App\Entity\Season;
use App\Repository\LevelRepository;
use App\Repository\SeasonRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

final class MembershipType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'member',
                TextType::class,
                [
                    'disabled' => true,
                    'label' => 'field.member',
                ]
            )
            ->add(
                'season',
                EntityType::class,
                [
                    'class' => Season::class,
                    'label' => 'field.season',
                    'query_builder' => static function (SeasonRepository $seasonRepository): QueryBuilder {
                        return $seasonRepository
                            ->createQueryBuilder('season')
                            ->addOrderBy('season.start', 'DESC');
                    },
                    'required' => true,
                ]
            )
            ->add(
                'level',
                EntityType::class,
                [
                    'class' => Level::class,
                    'label' => 'field.level',
                    'query_builder' => static function (LevelRepository $levelRepository): QueryBuilder {
                        return $levelRepository
                            ->createQueryBuilder('level')
                            ->addOrderBy('level.name', 'ASC');
                    },
                    'required' => true,
                ]
            )
            ->add(
                'number',
                TextType::class,
                [
                    'constraints' => [
                        new Length(min: 5, max: 5),
                    ],
                    'label' => 'field.number',
                    'required' => false,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Membership::class,
        ]);
    }
}
