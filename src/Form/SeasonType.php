<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Season;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

final class SeasonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'start',
                DateType::class,
                [
                    'constraints' => [
                        new NotNull(),
                    ],
                    'input' => 'datetime_immutable',
                    'label' => 'field.start',
                    'required' => true,
                    'widget' => 'single_text',
                ]
            )
            ->add(
                'end',
                DateType::class,
                [
                    'constraints' => [
                        new NotNull(),
                    ],
                    'input' => 'datetime_immutable',
                    'label' => 'field.end',
                    'required' => true,
                    'widget' => 'single_text',
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Season::class,
        ]);
    }
}
