<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class TransactionSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'thirdName',
                TextType::class,
                [
                    'attr' => [
                        'list' => 'third_names',
                    ],
                    'label' => 'field.third_name',
                    'required' => false,
                ]
            )
            ->add(
                'category',
                TextType::class,
                [
                    'attr' => [
                        'list' => 'categories',
                    ],
                    'label' => 'field.category',
                    'required' => false,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return '';
    }
}
