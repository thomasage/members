<?php

declare(strict_types=1);

namespace App\Form;

use App\Repository\LevelRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class LessonSearchType extends AbstractType
{
    private LevelRepository $levelRepository;

    public function __construct(LevelRepository $levelRepository)
    {
        $this->levelRepository = $levelRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $levels = [];
        foreach ($this->levelRepository->findBy([], ['name' => 'ASC']) as $level) {
            $levels[(string) $level] = (string) $level->getUuid();
        }

        $builder
            ->add(
                'level',
                ChoiceType::class,
                [
                    'choices' => $levels,
                    'label' => 'field.level',
                    'required' => false,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return '';
    }
}
