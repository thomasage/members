<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class StatisticsMembershipNumberType extends AbstractType
{
    use SeasonTrait;

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $this->addSeasonChoice($builder);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'method' => Request::METHOD_GET,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return '';
    }
}
