<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\PaymentMethod;
use App\Entity\Transaction;
use App\Repository\PaymentMethodRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;

class TransactionAddType extends AbstractType
{
    private string $currency;

    public function __construct(string $currency)
    {
        $this->currency = $currency;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'dateOperation',
                DateType::class,
                [
                    'input' => 'datetime_immutable',
                    'label' => 'field.date_operation',
                    'required' => true,
                    'widget' => 'single_text',
                ]
            )
            ->add(
                'dateValue',
                DateType::class,
                [
                    'input' => 'datetime_immutable',
                    'label' => 'field.date_value',
                    'required' => false,
                    'widget' => 'single_text',
                ]
            )
            ->add(
                'amountInCents',
                MoneyType::class,
                [
                    'currency' => $this->currency,
                    'divisor' => 100,
                    'label' => 'field.amount',
                    'required' => true,
                ]
            )
            ->add(
                'paymentMethod',
                EntityType::class,
                [
                    'class' => PaymentMethod::class,
                    'label' => 'field.payment_method',
                    'query_builder' => static fn (PaymentMethodRepository $paymentMethodRepository
                    ): QueryBuilder => $paymentMethodRepository
                        ->createQueryBuilder('payment_method')
                        ->orderBy('payment_method.name'),
                    'required' => true,
                ]
            )
            ->add(
                'thirdName',
                TextType::class,
                [
                    'attr' => [
                        'list' => 'third_names',
                    ],
                    'constraints' => [
                        new Length(min: 1, max: 100),
                        new NotNull(),
                    ],
                    'label' => 'field.third_name',
                    'required' => true,
                ]
            )
            ->add(
                'bankName',
                TextType::class,
                [
                    'attr' => [
                        'list' => 'bank_names',
                    ],
                    'constraints' => [
                        new Length(min: 1, max: 100),
                    ],
                    'label' => 'field.bank_name',
                    'required' => false,
                ]
            )
            ->add(
                'operationNumber',
                TextType::class,
                [
                    'constraints' => [
                        new Length(min: 1, max: 100),
                    ],
                    'label' => 'field.operation_number',
                    'required' => false,
                ]
            )
            ->add(
                'comment',
                TextType::class,
                [
                    'constraints' => [
                        new Length(min: 1, max: 255),
                    ],
                    'label' => 'field.comment',
                    'required' => false,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Transaction::class,
        ]);
    }
}
