<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Attendance;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class AttendanceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, [$this, 'preSetData']);
    }

    public function preSetData(FormEvent $event): void
    {
        /** @var Attendance $attendance */
        $attendance = $event->getData();
        $event
            ->getForm()
            ->add(
                'state',
                ChoiceType::class,
                [
                    'choices' => [
                        'attendance.absent' => Attendance::STATE_ABSENT,
                        'attendance.apology' => Attendance::STATE_APOLOGY,
                        'attendance.present' => Attendance::STATE_PRESENT,
                    ],
                    'expanded' => true,
                    'label' => $attendance->getMember(),
                    'multiple' => false,
                    'required' => true,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Attendance::class,
        ]);
    }
}
