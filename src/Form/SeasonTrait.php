<?php

declare(strict_types=1);

namespace App\Form;

use App\Repository\SeasonRepository;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

trait SeasonTrait
{
    private SeasonRepository $seasonRepository;

    public function __construct(SeasonRepository $seasonRepository)
    {
        $this->seasonRepository = $seasonRepository;
    }

    public function addSeasonChoice(FormBuilderInterface $builder): void
    {
        $seasons = [];
        foreach ($this->seasonRepository->findBy([], ['start' => 'DESC']) as $season) {
            $seasons[(string) $season] = (string) $season->getUuid();
        }

        $builder
            ->add(
                'season',
                ChoiceType::class,
                [
                    'choices' => $seasons,
                    'expanded' => false,
                    'label' => 'field.season',
                    'multiple' => false,
                    'required' => true,
                ]
            );
    }
}
