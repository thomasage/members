<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Attendance;
use App\Entity\Lesson;
use App\Entity\Theme;
use App\Repository\MemberRepository;
use App\Repository\ThemeRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

final class LessonAttendanceType extends AbstractType
{
    private MemberRepository $memberRepository;

    public function __construct(MemberRepository $memberRepository)
    {
        $this->memberRepository = $memberRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'themes',
                EntityType::class,
                [
                    'attr' => [
                        'class' => 'lesson_themes',
                    ],
                    'class' => Theme::class,
                    'expanded' => true,
                    'label' => 'field.themes',
                    'multiple' => true,
                    'query_builder' => static function (ThemeRepository $themeRepository): QueryBuilder {
                        return $themeRepository->createQueryBuilder('theme')->addOrderBy('theme.name', 'ASc');
                    },
                    'required' => false,
                ]
            )
            ->add(
                'comment',
                TextType::class,
                [
                    'constraints' => [
                        new Length(min: 1, max: 255),
                    ],
                    'label' => 'field.comment',
                    'required' => false,
                ]
            )
            ->addEventListener(FormEvents::PRE_SET_DATA, [$this, 'preSetData']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Lesson::class,
        ]);
    }

    public function preSetData(FormEvent $event): void
    {
        /** @var Lesson $lesson */
        $lesson = $event->getData();

        $attendances = $lesson->getAttendances();
        $members = $this->memberRepository->findAvailableByLesson($lesson);

        $collection = [];
        foreach ($members as $member) {
            $collection[(string) $member->getUuid()] = Attendance::createFromLessonAndMember($lesson, $member);
        }
        foreach ($attendances as $attendance) {
            $member = $attendance->getMember();
            $collection[(string) $member->getUuid()] = $attendance;
        }

        $event->getForm()->add(
            'attendances',
            CollectionType::class,
            [
                'data' => $collection,
                'entry_options' => [
                    'label' => false,
                ],
                'entry_type' => AttendanceType::class,
                'label' => 'field.members',
            ]
        );
    }
}
