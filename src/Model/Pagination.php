<?php

declare(strict_types=1);

namespace App\Model;

use Doctrine\ORM\Tools\Pagination\Paginator;
use Iterator;

final class Pagination implements Iterator
{
    private int $currentPage;
    private int $index = 0;
    /** @var int[] */
    private array $pages;
    /** @var array<string,mixed> */
    private array $queryString;
    private string $route;
    private int $totalPages;

    public function __construct(Paginator $paginator, Search $search, string $route)
    {
        $this->totalPages = (int) ceil(count($paginator) / $search->getMaxResultsPerPage());
        $this->currentPage = $search->getPage();
        $this->pages = range(max(0, $search->getPage() - 5), min($this->totalPages - 1, $search->getPage() + 5));
        $this->route = $route;
        $this->queryString = $search->getFilters();
    }

    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    public function getTotalPages(): int
    {
        return $this->totalPages;
    }

    public function getRoute(): string
    {
        return $this->route;
    }

    public function current(): int
    {
        return $this->pages[$this->index];
    }

    public function next(): void
    {
        ++$this->index;
    }

    public function key(): int
    {
        return $this->index;
    }

    public function valid(): bool
    {
        return isset($this->pages[$this->index]);
    }

    public function rewind(): void
    {
        $this->index = 0;
    }

    public function isPreviousPageAvailable(): bool
    {
        return $this->currentPage > 0;
    }

    public function isNextPageAvailable(): bool
    {
        return $this->currentPage < $this->totalPages - 1;
    }

    public function getLastPage(): int
    {
        return $this->totalPages - 1;
    }

    /**
     * @return array<string,mixed>
     */
    public function getQueryString(): array
    {
        return $this->queryString;
    }
}
