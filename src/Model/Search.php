<?php

declare(strict_types=1);

namespace App\Model;

final class Search
{
    /** @var array<string,mixed>|null */
    private ?array $filters = [];
    private int $maxResultsPerPage = 20;
    private int $page;

    public function __construct(int $page = 0)
    {
        $this->page = $page;
    }

    public function getMaxResultsPerPage(): int
    {
        return $this->maxResultsPerPage;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getFilter(string $code): mixed
    {
        return $this->filters[$code] ?? null;
    }

    /**
     * @return array<string,mixed>
     */
    public function getFilters(): array
    {
        return $this->filters ?? [];
    }

    /**
     * @param array<string,mixed>|null $data
     */
    public function setFilters(?array $data = null): void
    {
        $this->filters = $data;
    }
}
