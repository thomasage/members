<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\AttendanceRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: AttendanceRepository::class)]
#[UniqueEntity(['lesson', 'member'])]
class Attendance
{
    public const STATE_ABSENT = 0;
    public const STATE_APOLOGY = 1;
    public const STATE_PRESENT = 2;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer', options: ['unsigned' => true])]
    private ?int $id = null;

    #[ORM\Column(type: 'uuid', unique: true)]
    private Uuid $uuid;

    #[ORM\ManyToOne(targetEntity: Lesson::class, inversedBy: 'attendances')]
    #[ORM\JoinColumn(nullable: false)]
    private Lesson $lesson;

    #[ORM\ManyToOne(targetEntity: Member::class)]
    #[ORM\JoinColumn(nullable: false)]
    private Member $member;

    #[ORM\Column(type: 'smallint')]
    private int $state = self::STATE_ABSENT;

    public function __construct()
    {
        $this->uuid = Uuid::v4();
    }

    public static function createFromLessonAndMember(Lesson $lesson, Member $member): self
    {
        $attendance = new Attendance();
        $attendance
            ->setLesson($lesson)
            ->setMember($member);

        return $attendance;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    public function getLesson(): ?Lesson
    {
        return $this->lesson;
    }

    public function setLesson(Lesson $lesson): self
    {
        $this->lesson = $lesson;

        return $this;
    }

    public function getMember(): Member
    {
        return $this->member;
    }

    public function setMember(Member $member): self
    {
        $this->member = $member;

        return $this;
    }

    public function getState(): int
    {
        return $this->state;
    }

    public function setState(int $state): self
    {
        $this->state = $state;

        return $this;
    }
}
