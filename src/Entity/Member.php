<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\MemberRepository;
use DateTime;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Stringable;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: MemberRepository::class)]
class Member implements Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'smallint', options: ['unsigned' => true])]
    private ?int $id = null;

    #[ORM\Column(type: 'uuid', unique: true)]
    private Uuid $uuid;

    #[ORM\Column(type: 'string', length: 100)]
    private string $firstName;

    #[ORM\Column(type: 'string', length: 100)]
    private string $lastName;

    #[ORM\Column(type: 'string', length: 1)]
    private string $gender;

    #[ORM\Column(type: 'date_immutable')]
    private DateTimeImmutable $birthday;

    #[ORM\Column(type: 'string', length: 100)]
    private string $streetAddress;

    #[ORM\Column(type: 'string', length: 10)]
    private string $postCode;

    #[ORM\Column(type: 'string', length: 100)]
    private string $city;

    #[ORM\Column(type: 'string', length: 20, nullable: true)]
    private ?string $phone0 = null;

    #[ORM\Column(type: 'string', length: 20, nullable: true)]
    private ?string $phone1 = null;

    #[ORM\Column(type: 'string', length: 20, nullable: true)]
    private ?string $phone2 = null;

    #[ORM\Column(type: 'string', length: 20, nullable: true)]
    private ?string $phone3 = null;

    #[ORM\OneToMany(mappedBy: 'member', targetEntity: Membership::class)]
    private Collection $memberships;

    #[ORM\OneToMany(mappedBy: 'member', targetEntity: Promotion::class)]
    #[ORM\OrderBy(['nominationDate' => 'ASC'])]
    private Collection $promotions;

    public function __construct()
    {
        $this->memberships = new ArrayCollection();
        $this->promotions = new ArrayCollection();
        $this->uuid = Uuid::v4();
    }

    public function __toString(): string
    {
        return "$this->firstName $this->lastName";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getBirthday(): DateTimeImmutable
    {
        return $this->birthday;
    }

    public function setBirthday(DateTimeImmutable $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getAge(): int
    {
        return (new DateTime())->diff($this->birthday)->y;
    }

    public function getStreetAddress(): string
    {
        return $this->streetAddress;
    }

    public function setStreetAddress(string $streetAddress): self
    {
        $this->streetAddress = $streetAddress;

        return $this;
    }

    public function getPostCode(): string
    {
        return $this->postCode;
    }

    public function setPostCode(string $postCode): self
    {
        $this->postCode = $postCode;

        return $this;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPhone0(): ?string
    {
        return $this->phone0;
    }

    public function setPhone0(?string $phone0): self
    {
        $this->phone0 = $phone0;

        return $this;
    }

    public function getPhone1(): ?string
    {
        return $this->phone1;
    }

    public function setPhone1(?string $phone1): self
    {
        $this->phone1 = $phone1;

        return $this;
    }

    public function getPhone2(): ?string
    {
        return $this->phone2;
    }

    public function setPhone2(?string $phone2): self
    {
        $this->phone2 = $phone2;

        return $this;
    }

    public function getPhone3(): ?string
    {
        return $this->phone3;
    }

    public function setPhone3(?string $phone3): self
    {
        $this->phone3 = $phone3;

        return $this;
    }

    /**
     * @return Collection|Membership[]
     */
    public function getMemberships(): ArrayCollection|Collection
    {
        return $this->memberships;
    }

    public function getGender(): string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getLastPromotion(): ?Promotion
    {
        $last = $this->promotions->last();

        return false === $last ? null : $last;
    }
}
