<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\PromotionRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: PromotionRepository::class)]
#[UniqueEntity(['member', 'rank'])]
class Promotion
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer', options: ['unsigned' => true])]
    private ?int $id = null;

    #[ORM\Column(type: 'uuid', unique: true)]
    private Uuid $uuid;

    #[ORM\ManyToOne(targetEntity: Member::class, inversedBy: 'promotions')]
    #[ORM\JoinColumn(nullable: false)]
    private Member $member;

    #[ORM\ManyToOne(targetEntity: Rank::class)]
    #[ORM\JoinColumn(nullable: false)]
    private Rank $rank;

    #[ORM\Column(type: 'date')]
    private DateTimeInterface $nominationDate;

    public function __construct()
    {
        $this->uuid = Uuid::v4();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    public function getMember(): Member
    {
        return $this->member;
    }

    public function setMember(Member $member): self
    {
        $this->member = $member;

        return $this;
    }

    public function getRank(): Rank
    {
        return $this->rank;
    }

    public function setRank(Rank $rank): self
    {
        $this->rank = $rank;

        return $this;
    }

    public function getNominationDate(): ?DateTimeInterface
    {
        return $this->nominationDate;
    }

    public function setNominationDate(DateTimeInterface $nominationDate): self
    {
        $this->nominationDate = $nominationDate;

        return $this;
    }
}
