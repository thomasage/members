<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\TransactionRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: TransactionRepository::class)]
class Transaction
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer', options: ['unsigned' => true])]
    private ?int $id = null;

    #[ORM\Column(type: 'uuid', unique: true)]
    private Uuid $uuid;

    #[ORM\ManyToOne(targetEntity: PaymentMethod::class)]
    #[ORM\JoinColumn(nullable: false)]
    private PaymentMethod $paymentMethod;

    #[ORM\Column(type: 'date_immutable')]
    private DateTimeImmutable $dateOperation;

    #[ORM\Column(type: 'date_immutable', nullable: true)]
    private ?DateTimeImmutable $dateValue = null;

    #[ORM\Column(type: 'integer')]
    private int $amountInCents;

    #[ORM\Column(type: 'string', length: 100)]
    private string $thirdName;

    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    private ?string $bankName = null;

    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    private ?string $operationNumber = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $comment = null;

    #[ORM\OneToMany(
        mappedBy: 'transaction',
        targetEntity: TransactionDetail::class,
        cascade: ['persist', 'remove']
    )]
    private Collection $details;

    public function __construct()
    {
        $this->details = new ArrayCollection();
        $this->uuid = Uuid::v4();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    public function getPaymentMethod(): PaymentMethod
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(PaymentMethod $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    public function getDateOperation(): DateTimeImmutable
    {
        return $this->dateOperation;
    }

    public function setDateOperation(DateTimeImmutable $dateOperation): self
    {
        $this->dateOperation = $dateOperation;

        return $this;
    }

    public function getDateValue(): ?DateTimeImmutable
    {
        return $this->dateValue;
    }

    public function setDateValue(?DateTimeImmutable $dateValue): self
    {
        $this->dateValue = $dateValue;

        return $this;
    }

    public function getAmountInCents(): int
    {
        return $this->amountInCents;
    }

    public function setAmountInCents(int $amountInCents): self
    {
        $this->amountInCents = $amountInCents;

        return $this;
    }

    public function getThirdName(): string
    {
        return $this->thirdName;
    }

    public function setThirdName(string $thirdName): self
    {
        $this->thirdName = $thirdName;

        return $this;
    }

    public function getBankName(): ?string
    {
        return $this->bankName;
    }

    public function setBankName(?string $bankName): self
    {
        $this->bankName = $bankName;

        return $this;
    }

    public function getOperationNumber(): ?string
    {
        return $this->operationNumber;
    }

    public function setOperationNumber(?string $operationNumber): self
    {
        $this->operationNumber = $operationNumber;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return Collection<TransactionDetail>
     */
    public function getDetails(): Collection
    {
        return $this->details;
    }

    public function addDetail(TransactionDetail $detail): self
    {
        $detail->setTransaction($this);
        $this->details->add($detail);

        return $this;
    }

    public function removeDetail(TransactionDetail $detail): self
    {
        $this->details->removeElement($detail);

        return $this;
    }

    /**
     * @return array<string,int>
     */
    public function getTotalByCategory(): array
    {
        $total = [];
        /** @var TransactionDetail $detail */
        foreach ($this->details as $detail) {
            $category = $detail->getCategory();
            $total[$category] = ($total[$category] ?? 0) + $detail->getAmountInCents();
        }

        return $total;
    }
}
