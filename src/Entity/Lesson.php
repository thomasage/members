<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\LessonRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: LessonRepository::class)]
class Lesson
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer', options: ['unsigned' => true])]
    private ?int $id = null;

    #[ORM\Column(type: 'uuid', unique: true)]
    private Uuid $uuid;

    #[ORM\Column(type: 'datetime_immutable')]
    private DateTimeImmutable $startAt;

    #[ORM\Column(type: 'smallint', options: ['unsigned' => true])]
    private int $durationInMinutes = 90;

    #[ORM\Column(type: 'boolean')]
    private bool $active = true;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $comment = null;

    #[ORM\OneToMany(
        mappedBy: 'lesson',
        targetEntity: Attendance::class,
        cascade: ['persist', 'remove'],
        orphanRemoval: true
    )]
    private Collection $attendances;

    #[ORM\ManyToMany(targetEntity: Level::class)]
    private Collection $levels;

    #[ORM\ManyToMany(targetEntity: Theme::class)]
    private Collection $themes;

    /** @var array<int,int> */
    private array $countByState = [];

    public function __construct()
    {
        $this->attendances = new ArrayCollection();
        $this->levels = new ArrayCollection();
        $this->themes = new ArrayCollection();
        $this->uuid = Uuid::v4();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    public function getStartAt(): DateTimeImmutable
    {
        return $this->startAt;
    }

    public function setStartAt(DateTimeImmutable $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getDurationInMinutes(): int
    {
        return $this->durationInMinutes;
    }

    public function setDurationInMinutes(int $durationInMinutes): self
    {
        $this->durationInMinutes = $durationInMinutes;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return Collection|Level[]
     */
    public function getLevels(): Collection
    {
        return $this->levels;
    }

    public function addLevel(Level $level): self
    {
        if (!$this->levels->contains($level)) {
            $this->levels[] = $level;
        }

        return $this;
    }

    public function removeLevel(Level $level): self
    {
        $this->levels->removeElement($level);

        return $this;
    }

    /**
     * @return Collection|Theme[]
     */
    public function getThemes(): Collection
    {
        return $this->themes;
    }

    public function setThemes(Theme ...$themes): self
    {
        $this->themes = new ArrayCollection($themes);

        return $this;
    }

    public function addTheme(Theme $theme): self
    {
        if (!$this->themes->contains($theme)) {
            $this->themes[] = $theme;
        }

        return $this;
    }

    public function removeTheme(Theme $theme): self
    {
        $this->themes->removeElement($theme);

        return $this;
    }

    /**
     * @return Collection|Attendance[]
     */
    public function getAttendances(): Collection
    {
        return $this->attendances;
    }

    public function addAttendance(Attendance $attendance): self
    {
        if (!$this->attendances->contains($attendance)) {
            $this->attendances->add($attendance);
        }

        return $this;
    }

    public function removeAttendance(Attendance $attendance): self
    {
        if ($this->attendances->contains($attendance)) {
            $this->attendances->removeElement($attendance);
        }

        return $this;
    }

    public function getCountPresent(): int
    {
        $this->computeCountByState();

        return $this->countByState[Attendance::STATE_PRESENT];
    }

    public function computeCountByState(): void
    {
        if (count($this->countByState) > 0) {
            return;
        }
        $this->countByState = [
            Attendance::STATE_ABSENT => 0,
            Attendance::STATE_APOLOGY => 0,
            Attendance::STATE_PRESENT => 0,
        ];
        foreach ($this->attendances as $attendance) {
            ++$this->countByState[(int) $attendance->getState()];
        }
    }

    public function getCountApology(): int
    {
        $this->computeCountByState();

        return $this->countByState[Attendance::STATE_APOLOGY];
    }

    public function getCountAbsent(): int
    {
        $this->computeCountByState();

        return $this->countByState[Attendance::STATE_ABSENT];
    }

    public function isFuture(): bool
    {
        return $this->startAt > new DateTimeImmutable();
    }
}
