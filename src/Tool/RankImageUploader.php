<?php

declare(strict_types=1);

namespace App\Tool;

use App\Entity\Rank;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final class RankImageUploader
{
    public function __construct(
        private readonly string $rankImageDirectory,
        private readonly string $publicDirectory,
    ) {
    }

    public function upload(Rank $rank, UploadedFile $image): void
    {
        $image->move($this->publicDirectory.'/'.$this->rankImageDirectory, $rank->getUuid().'.png');
    }
}
