<?php

declare(strict_types=1);

namespace App\Twig;

use App\Entity\Rank;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

final class RankImageExtension extends AbstractExtension
{
    public function __construct(
        private readonly string $rankImageDirectory,
        private readonly string $publicDirectory,
    ) {
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('rank_image', [$this, 'rankImage']),
        ];
    }

    public function rankImage(Rank $rank): string
    {
        $filename = $this->publicDirectory.'/'.$this->rankImageDirectory.'/'.$rank->getUuid().'.png';
        $image = file_exists($filename) ? $rank->getUuid().'.png' : 'empty.png';

        return $this->rankImageDirectory.'/'.$image;
    }
}
