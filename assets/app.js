import './bootstrap'

import './styles/app.scss'
import 'bootstrap/dist/js/bootstrap.min'

document
  .querySelectorAll('tr[data-url]')
  .forEach(element => {
    element.addEventListener('click', event => {
      location.href = event.currentTarget.dataset.url
    })
  })

document
  .querySelectorAll('[data-auto-submit]')
  .forEach(element => {
    element.addEventListener('change', event => {
      event.target.form.submit()
    })
  })

document
  .querySelectorAll('#transaction_detail_add')
  .forEach(element =>
    element.addEventListener('click', () => {
      const collection = document.getElementById('transaction_edit_details')
      const item = document.createElement('tr')
      item.innerHTML = collection.dataset.prototype.replace(/__name__/g, collection.dataset.index)
      collection.appendChild(item)
      collection.dataset.index++
    })
  )

document
  .querySelectorAll('.transaction_detail_delete')
  .forEach(element => element.addEventListener('click', event => {
      event.preventDefault()
      element.closest('tr').remove()
    })
  )

document
  .querySelectorAll('.lesson_attendances > div > label')
  .forEach(element => element.addEventListener('click', event => {
    event.preventDefault()
    const inputs = Array.from(element.parentNode.querySelectorAll('input[type=radio]'))
    const selectedInput = inputs.find(input => input.checked)
    if (!selectedInput) {
      return
    }
    const selectedValue = parseInt(selectedInput.value)
    const nextValue = selectedValue === 2 ? 0 : selectedValue + 1
    const nextInput = inputs.find(input => parseInt(input.value) === nextValue)
    if (!nextInput) {
      return
    }
    nextInput.click()
  }))
