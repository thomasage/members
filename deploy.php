<?php

declare(strict_types=1);

namespace Deployer;

require 'recipe/symfony.php';

// Config

set('keep_releases', 1);
set('repository', 'git@gitlab.com:thomasage/members.git');

add('shared_files', []);
add('shared_dirs', ['public/images']);
add('writable_dirs', []);

// Hosts

import('hosts.yaml');

// Tasks

desc('Deploy assets');
task(
    'deploy:assets',
    static function () {
        runLocally('yarn run encore prod');
        upload('public/build', '{{release_path}}/public');
    }
);

after('deploy:vendors', 'deploy:assets');

after('deploy:assets', 'database:migrate');

after('deploy:failed', 'deploy:unlock');
