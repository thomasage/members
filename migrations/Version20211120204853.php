<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211120204853 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql(
            'CREATE TABLE payment_method (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', name VARCHAR(50) NOT NULL, UNIQUE INDEX UNIQ_7B61A1F6D17F50A6 (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );
        $this->addSql(
            'CREATE TABLE transaction (id INT UNSIGNED AUTO_INCREMENT NOT NULL, payment_method_id SMALLINT UNSIGNED NOT NULL, uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', date_operation DATE NOT NULL COMMENT \'(DC2Type:date_immutable)\', date_value DATE DEFAULT NULL COMMENT \'(DC2Type:date_immutable)\', amount_in_cents INT NOT NULL, third_name VARCHAR(100) NOT NULL, bank_name VARCHAR(100) DEFAULT NULL, operation_number VARCHAR(100) DEFAULT NULL, comment VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_723705D1D17F50A6 (uuid), INDEX IDX_723705D15AA1164F (payment_method_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );
        $this->addSql(
            'CREATE TABLE transaction_detail (id INT UNSIGNED AUTO_INCREMENT NOT NULL, transaction_id INT UNSIGNED NOT NULL, uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', category VARCHAR(100) NOT NULL, amount_in_cents INT NOT NULL, comment VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_587B0DD3D17F50A6 (uuid), INDEX IDX_587B0DD32FC0CB0F (transaction_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );
        $this->addSql(
            'ALTER TABLE transaction ADD CONSTRAINT FK_723705D15AA1164F FOREIGN KEY (payment_method_id) REFERENCES payment_method (id)'
        );
        $this->addSql(
            'ALTER TABLE transaction_detail ADD CONSTRAINT FK_587B0DD32FC0CB0F FOREIGN KEY (transaction_id) REFERENCES transaction (id)'
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D15AA1164F');
        $this->addSql('ALTER TABLE transaction_detail DROP FOREIGN KEY FK_587B0DD32FC0CB0F');
        $this->addSql('DROP TABLE payment_method');
        $this->addSql('DROP TABLE transaction');
        $this->addSql('DROP TABLE transaction_detail');
    }
}
