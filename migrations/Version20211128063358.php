<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211128063358 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql(
            <<<'SQL'
ALTER TABLE rank 
    CHANGE description description VARCHAR(100) NOT NULL,
    CHANGE lessons lessons SMALLINT UNSIGNED DEFAULT NULL,
    CHANGE age_min age_min SMALLINT UNSIGNED DEFAULT NULL,
    CHANGE age_max age_max SMALLINT UNSIGNED DEFAULT NULL
SQL
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql(
            <<<'SQL'
ALTER TABLE rank 
    CHANGE description description VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, 
    CHANGE lessons lessons SMALLINT DEFAULT NULL,
    CHANGE age_min age_min SMALLINT DEFAULT NULL,
    CHANGE age_max age_max SMALLINT DEFAULT NULL
SQL
        );
    }
}
