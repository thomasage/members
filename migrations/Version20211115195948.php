<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211115195948 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql(
            'CREATE TABLE attendance (id INT UNSIGNED AUTO_INCREMENT NOT NULL, lesson_id INT UNSIGNED NOT NULL, member_id SMALLINT UNSIGNED NOT NULL, uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', state SMALLINT NOT NULL, UNIQUE INDEX UNIQ_6DE30D91D17F50A6 (uuid), INDEX IDX_6DE30D91CDF80196 (lesson_id), INDEX IDX_6DE30D917597D3FE (member_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );
        $this->addSql(
            'CREATE TABLE lesson (id INT UNSIGNED AUTO_INCREMENT NOT NULL, uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', start_at DATETIME NOT NULL, duration_in_minutes SMALLINT UNSIGNED NOT NULL, active TINYINT(1) NOT NULL, comment VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_F87474F3D17F50A6 (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );
        $this->addSql(
            'CREATE TABLE lesson_level (lesson_id INT UNSIGNED NOT NULL, level_id SMALLINT UNSIGNED NOT NULL, INDEX IDX_D00C1894CDF80196 (lesson_id), INDEX IDX_D00C18945FB14BA7 (level_id), PRIMARY KEY(lesson_id, level_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );
        $this->addSql(
            'CREATE TABLE lesson_theme (lesson_id INT UNSIGNED NOT NULL, theme_id SMALLINT UNSIGNED NOT NULL, INDEX IDX_DD93338FCDF80196 (lesson_id), INDEX IDX_DD93338F59027487 (theme_id), PRIMARY KEY(lesson_id, theme_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );
        $this->addSql(
            'CREATE TABLE level (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', name VARCHAR(100) NOT NULL, UNIQUE INDEX UNIQ_9AEACC13D17F50A6 (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );
        $this->addSql(
            'CREATE TABLE member (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', first_name VARCHAR(100) NOT NULL, last_name VARCHAR(100) NOT NULL, birthday DATE NOT NULL, street_address VARCHAR(100) NOT NULL, post_code VARCHAR(10) NOT NULL, city VARCHAR(100) NOT NULL, phone0 VARCHAR(20) DEFAULT NULL, phone1 VARCHAR(20) DEFAULT NULL, phone2 VARCHAR(20) DEFAULT NULL, phone3 VARCHAR(20) DEFAULT NULL, UNIQUE INDEX UNIQ_70E4FA78D17F50A6 (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );
        $this->addSql(
            'CREATE TABLE membership (id INT UNSIGNED AUTO_INCREMENT NOT NULL, member_id SMALLINT UNSIGNED NOT NULL, season_id SMALLINT UNSIGNED NOT NULL, level_id SMALLINT UNSIGNED DEFAULT NULL, uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', number VARCHAR(5) DEFAULT NULL, UNIQUE INDEX UNIQ_86FFD285D17F50A6 (uuid), INDEX IDX_86FFD2857597D3FE (member_id), INDEX IDX_86FFD2854EC001D1 (season_id), INDEX IDX_86FFD2855FB14BA7 (level_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );
        $this->addSql(
            'CREATE TABLE promotion (id INT UNSIGNED AUTO_INCREMENT NOT NULL, member_id SMALLINT UNSIGNED NOT NULL, rank_id SMALLINT UNSIGNED NOT NULL, uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', nomination_date DATE NOT NULL, UNIQUE INDEX UNIQ_C11D7DD1D17F50A6 (uuid), INDEX IDX_C11D7DD17597D3FE (member_id), INDEX IDX_C11D7DD17616678F (rank_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );
        $this->addSql(
            'CREATE TABLE rank (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', name VARCHAR(100) NOT NULL, description VARCHAR(100) DEFAULT NULL, position SMALLINT NOT NULL, lessons SMALLINT DEFAULT NULL, age_min SMALLINT DEFAULT NULL, age_max SMALLINT DEFAULT NULL, UNIQUE INDEX UNIQ_8879E8E5D17F50A6 (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );
        $this->addSql(
            'CREATE TABLE season (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', start DATE NOT NULL, end DATE NOT NULL, UNIQUE INDEX UNIQ_F0E45BA9D17F50A6 (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );
        $this->addSql(
            'CREATE TABLE theme (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', name VARCHAR(100) NOT NULL, UNIQUE INDEX UNIQ_9775E708D17F50A6 (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );
        $this->addSql(
            'CREATE TABLE `user` (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', username VARCHAR(180) NOT NULL, roles LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649D17F50A6 (uuid), UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );
        $this->addSql(
            'ALTER TABLE attendance ADD CONSTRAINT FK_6DE30D91CDF80196 FOREIGN KEY (lesson_id) REFERENCES lesson (id)'
        );
        $this->addSql(
            'ALTER TABLE attendance ADD CONSTRAINT FK_6DE30D917597D3FE FOREIGN KEY (member_id) REFERENCES member (id)'
        );
        $this->addSql(
            'ALTER TABLE lesson_level ADD CONSTRAINT FK_D00C1894CDF80196 FOREIGN KEY (lesson_id) REFERENCES lesson (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE lesson_level ADD CONSTRAINT FK_D00C18945FB14BA7 FOREIGN KEY (level_id) REFERENCES level (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE lesson_theme ADD CONSTRAINT FK_DD93338FCDF80196 FOREIGN KEY (lesson_id) REFERENCES lesson (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE lesson_theme ADD CONSTRAINT FK_DD93338F59027487 FOREIGN KEY (theme_id) REFERENCES theme (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE membership ADD CONSTRAINT FK_86FFD2857597D3FE FOREIGN KEY (member_id) REFERENCES member (id)'
        );
        $this->addSql(
            'ALTER TABLE membership ADD CONSTRAINT FK_86FFD2854EC001D1 FOREIGN KEY (season_id) REFERENCES season (id)'
        );
        $this->addSql(
            'ALTER TABLE membership ADD CONSTRAINT FK_86FFD2855FB14BA7 FOREIGN KEY (level_id) REFERENCES level (id)'
        );
        $this->addSql(
            'ALTER TABLE promotion ADD CONSTRAINT FK_C11D7DD17597D3FE FOREIGN KEY (member_id) REFERENCES member (id)'
        );
        $this->addSql(
            'ALTER TABLE promotion ADD CONSTRAINT FK_C11D7DD17616678F FOREIGN KEY (rank_id) REFERENCES rank (id)'
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE attendance DROP FOREIGN KEY FK_6DE30D91CDF80196');
        $this->addSql('ALTER TABLE lesson_level DROP FOREIGN KEY FK_D00C1894CDF80196');
        $this->addSql('ALTER TABLE lesson_theme DROP FOREIGN KEY FK_DD93338FCDF80196');
        $this->addSql('ALTER TABLE lesson_level DROP FOREIGN KEY FK_D00C18945FB14BA7');
        $this->addSql('ALTER TABLE membership DROP FOREIGN KEY FK_86FFD2855FB14BA7');
        $this->addSql('ALTER TABLE attendance DROP FOREIGN KEY FK_6DE30D917597D3FE');
        $this->addSql('ALTER TABLE membership DROP FOREIGN KEY FK_86FFD2857597D3FE');
        $this->addSql('ALTER TABLE promotion DROP FOREIGN KEY FK_C11D7DD17597D3FE');
        $this->addSql('ALTER TABLE promotion DROP FOREIGN KEY FK_C11D7DD17616678F');
        $this->addSql('ALTER TABLE membership DROP FOREIGN KEY FK_86FFD2854EC001D1');
        $this->addSql('ALTER TABLE lesson_theme DROP FOREIGN KEY FK_DD93338F59027487');
        $this->addSql('DROP TABLE attendance');
        $this->addSql('DROP TABLE lesson');
        $this->addSql('DROP TABLE lesson_level');
        $this->addSql('DROP TABLE lesson_theme');
        $this->addSql('DROP TABLE level');
        $this->addSql('DROP TABLE member');
        $this->addSql('DROP TABLE membership');
        $this->addSql('DROP TABLE promotion');
        $this->addSql('DROP TABLE rank');
        $this->addSql('DROP TABLE season');
        $this->addSql('DROP TABLE theme');
        $this->addSql('DROP TABLE `user`');
    }
}
