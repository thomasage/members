<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211206203031 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE user ADD admin TINYINT(1) NOT NULL');
        $this->addSql('UPDATE user SET admin = 1 WHERE roles = \'ROLE_ADMIN\'');
        $this->addSql('ALTER TABLE user DROP roles');
    }

    public function down(Schema $schema): void
    {
        $this->addSql(
            'ALTER TABLE `user` ADD roles LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:simple_array)\''
        );
        $this->addSql('UPDATE user SET roles = \'ROLE_ADMIN\' WHERE admin = 1');
        $this->addSql('ALTER TABLE `user` DROP admin');
    }
}
